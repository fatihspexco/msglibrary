package com.spexco.msgframework.ui.creategroup.info;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Contact;
import com.spexco.msgframework.ui.creategroup.participant.CreateGroupContactAdapter;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.spexco.msgframework.utils.Utility.isValid;

/**
 * Created by fatihinan on 04.12.2018.
 */
public class CreateGroupInfoContactAdapter extends RecyclerView.Adapter<CreateGroupInfoContactAdapter.ViewHolder> {

    private GroupInfoListener listener;

    CreateGroupInfoContactAdapter(GroupInfoListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_create_group_info_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = CreateGroupContactAdapter.selectedContacts.get(position);
        holder.mVisibleNameView.setText(holder.mItem.VisibleName);

        if ( isValid(holder.mItem.PhotoURL) ) {
            Glide
                .with(holder.mProfileImageView)
                .load(holder.mItem.PhotoURL)
                .into(holder.mProfileImageView);
        } else {
            Glide
                .with(holder.mProfileImageView)
                .load(R.drawable.ic_default_user)
                .into(holder.mProfileImageView);
        }

        holder.mRemoveImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateGroupContactAdapter.selectedContacts.remove(CreateGroupContactAdapter.selectedContacts.get(position));
                notifyDataSetChanged();
                if (listener != null) {
                    listener.onRemove();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return CreateGroupContactAdapter.selectedContacts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mVisibleNameView;
        public final CircleImageView mProfileImageView;
        public final CircleImageView mRemoveImageView;
        public Contact mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mVisibleNameView = view.findViewById(R.id.tv_visible_name);
            mRemoveImageView = view.findViewById(R.id.iv_remove);
            mProfileImageView = view.findViewById(R.id.iv_profile_image);
        }
    }

    public interface GroupInfoListener {
        void onRemove();
    }
}
