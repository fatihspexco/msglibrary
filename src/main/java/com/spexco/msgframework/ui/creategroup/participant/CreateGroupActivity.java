package com.spexco.msgframework.ui.creategroup.participant;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.spexco.msgframework.db.Contact;
import com.spexco.msgframework.ui.creategroup.info.CreateGroupInfoActivity;
import com.spexco.msgframework.utils.Consts;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CreateGroupActivity extends AppCompatActivity implements CreateGroupContactAdapter.ContactListListener {

    private CreateGroupContactAdapter adapter;
    private RecyclerView recyclerView;
    private EditText etSearch;
    private TextView tvCount, tvNext;
    private int contactsSize = 0;
    private List<Contact> contacts;
    public static SelectParticipantListener listener;
    public static List<String> selectedContacts;
    public static Activity previousActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        tvNext = findViewById(R.id.tv_next);
        tvCount = findViewById(R.id.tv_count);
        recyclerView = findViewById(R.id.rv_contact);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new CreateGroupContactAdapter(this);
        recyclerView.setAdapter(adapter);

        etSearch = findViewById(R.id.et_search);
        etSearch.addTextChangedListener(textWatcher);

        contacts = UserInfo.database.contactModel().getAllContacts(UserInfo.myUserID);

        if (listener != null) {
            tvNext.setText("Ekle");

            Iterator<Contact> iterator = contacts.iterator();
            while(iterator.hasNext()){
                Contact c = iterator.next();
                if (selectedContacts.contains(c.UserID)) {
                    iterator.remove();
                }
            }
        }

        contactsSize = contacts.size();
    }

    @Override
    protected void onResume() {
        super.onResume();
        etSearch.setText("");
        adapter.setContacts(contacts);
        tvCount.setText(CreateGroupContactAdapter.selectedContacts.size() + " / " + contactsSize);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { }

        @Override
        public void afterTextChanged(Editable s) {
            adapter.filter(s.toString());
        }
    };

    @Override
    public void onSelect(int count) {
        tvCount.setText(count + " / " + contactsSize);
        tvNext.setTextColor(count == 0 ? getResources().getColor(R.color.disabled) : getResources().getColor(R.color.colorPrimaryDark));
        tvNext.setEnabled(count > 0);
    }

    public void onNext(View view) {

        if (listener != null) {
            List<String> userIDs = new ArrayList<>();
            for (Contact c : CreateGroupContactAdapter.selectedContacts) {
                userIDs.add(c.UserID);
            }
            listener.onSelect(userIDs, Consts.VALUE_TASK_GROUP_ADD_PARTICIPANT);
            listener = null;
            finish();
        } else {
            Intent intent = new Intent(CreateGroupActivity.this, CreateGroupInfoActivity.class);
            CreateGroupInfoActivity.previousActivity = this;
            startActivity(intent);
        }
    }

    public void onCancel(View view) {
        listener = null;
        finish();
    }

    public interface SelectParticipantListener {
        void onSelect(List<String> userIDs, String task);
    }
}
