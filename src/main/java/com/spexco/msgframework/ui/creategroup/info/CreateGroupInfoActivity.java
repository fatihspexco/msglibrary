package com.spexco.msgframework.ui.creategroup.info;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.spexco.msgframework.db.Group;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Contact;
import com.spexco.msgframework.ui.creategroup.participant.CreateGroupActivity;
import com.spexco.msgframework.ui.creategroup.participant.CreateGroupContactAdapter;
import com.spexco.msgframework.utils.Utility;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.List;

import static com.spexco.msgframework.utils.Consts.*;
import static com.spexco.msgframework.utils.UserInfo.context;
import static com.spexco.msgframework.utils.UserInfo.database;
import static com.spexco.msgframework.utils.UserInfo.myUserID;
import static com.spexco.msgframework.utils.UserInfo.sendToSocket;

public class CreateGroupInfoActivity extends AppCompatActivity implements CreateGroupInfoContactAdapter.GroupInfoListener {

    private CreateGroupInfoContactAdapter adapter;
    private RecyclerView recyclerView;
    private TextView tvCreate, tvCount;
    private EditText etGroupName;
    private ImageView ivGroupImage;
    private int contactsSize = 0;
    private Uri fileUri;
    private ProgressDialog progress;
    private Group group = new Group();
    public static CreateGroupActivity previousActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group_info);

        ivGroupImage = findViewById(R.id.iv_group_image);
        etGroupName = findViewById(R.id.et_group_name);
        tvCount = findViewById(R.id.tv_count);
        tvCreate = findViewById(R.id.tv_create);
        recyclerView = findViewById(R.id.rv_contact);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        adapter = new CreateGroupInfoContactAdapter(this);
        recyclerView.setAdapter(adapter);

        List<Contact> contacts = database.contactModel().getAllContacts(myUserID);
        contactsSize = contacts.size();
        tvCount.setText("KATILIMCILAR: " + contactsSize + " KİŞİDEN " + CreateGroupContactAdapter.selectedContacts.size() + " KİŞİ");

        etGroupName.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override public void onTextChanged(CharSequence s, int start, int before, int count) { }
            @Override
            public void afterTextChanged(Editable s) {
                updateCreateEnabled();
            }
        });

        ivGroupImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CropImage.activity().setCropShape(CropImageView.CropShape.OVAL)
                        .setAspectRatio(1, 1)
                        .start(CreateGroupInfoActivity.this);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                fileUri = result.getUri();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Glide.with(context).load(fileUri).into(ivGroupImage);
                    }
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    public void onRemove() {
        tvCount.setText("KATILIMCILAR: " + contactsSize + " KİŞİDEN " + CreateGroupContactAdapter.selectedContacts.size() + " KİŞİ");
        updateCreateEnabled();
    }

    private void updateCreateEnabled() {
        int selectedSize = CreateGroupContactAdapter.selectedContacts.size();
        boolean createEnabled = etGroupName.getText().toString().length() > 0 && selectedSize > 0;
        tvCreate.setTextColor(createEnabled ? getResources().getColor(R.color.colorPrimaryDark) : getResources().getColor(R.color.disabled));
        tvCreate.setEnabled(createEnabled);
    }

    public void onCreate(View view) {

        if (fileUri != null) {
            progress = ProgressDialog.show(this, "", "Grup oluşturuluyor...");
            Utility.uploadFile(new Utility.UploadFileListener() {
                @Override
                public void onResponse(boolean success, String description) {
                    if (success) {
                        group.PhotoURL = description;
                        saveGroup();
                    }
                    fileUri = null;
                    progress.dismiss();
                }
            }, new File(fileUri.getPath()));
        } else {
            saveGroup();
        }
    }

    private void saveGroup() {

        group.GroupName = etGroupName.getText().toString();

        JsonObject messageObject = new JsonObject();
        messageObject.addProperty(KEY_TYPE, VALUE_TYPE_SET);
        messageObject.addProperty(KEY_TASK, VALUE_TASK_CREATE_GROUP);
        messageObject.addProperty(KEY_FROM, myUserID);
        messageObject.addProperty(KEY_OWNER, myUserID);
        messageObject.addProperty(KEY_GROUP_ID, group.GroupID);
        messageObject.addProperty(KEY_GROUP_NAME, group.GroupName);
        messageObject.addProperty(KEY_PHOTO_URL, group.PhotoURL);
        messageObject.addProperty(KEY_GROUP_DESCRIPTION, group.GroupDescription);

        JsonArray participantsArray = new JsonArray();
        for (Contact c : CreateGroupContactAdapter.selectedContacts) {
            participantsArray.add(c.UserID);
        }
        messageObject.add(KEY_PARTICIPANTS, participantsArray);

        sendToSocket(messageObject.toString());

        previousActivity.previousActivity.finish();
        previousActivity.finish();
        finish();
    }

    public void onBack(View view) {
        finish();
    }
}
