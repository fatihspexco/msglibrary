package com.spexco.msgframework.ui.creategroup.participant;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Contact;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.spexco.msgframework.utils.Utility.isValid;

/**
 * Created by fatihinan on 04.12.2018.
 */
public class CreateGroupContactAdapter extends RecyclerView.Adapter<CreateGroupContactAdapter.ViewHolder> {

    private List<Contact> contacts = new ArrayList<>();
    private List<Contact> filteredContacts = new ArrayList<>();
    private final ContactListListener mListener;
    public static List<Contact> selectedContacts = new ArrayList<>();

    public CreateGroupContactAdapter(ContactListListener listener) {
        selectedContacts = new ArrayList<>();
        mListener = listener;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
        filteredContacts.clear();
        filteredContacts.addAll(contacts);
    }

    public void filter(String name) {
        filteredContacts.clear();

        if (name.length() > 0) {
            for (Contact c : contacts) {
                if (c.VisibleName.toLowerCase().contains(name.toLowerCase())) {
                    filteredContacts.add(c);
                }
            }
        } else {
            filteredContacts.addAll(contacts);
        }

        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_create_group_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = filteredContacts.get(position);
        holder.mVisibleNameView.setText(holder.mItem.VisibleName);
        holder.mStatusTextView.setText(holder.mItem.StatusText);

        holder.cbSelected.setChecked(selectedContacts.contains(holder.mItem));

        if ( isValid(filteredContacts.get(position).PhotoURL) ) {
            Glide
                .with(holder.mProfileImageView)
                .load(filteredContacts.get(position).PhotoURL)
                .into(holder.mProfileImageView);
        } else {
            Glide
                    .with(holder.mProfileImageView)
                    .load(R.drawable.ic_default_user)
                    .into(holder.mProfileImageView);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.cbSelected.setChecked(!holder.cbSelected.isChecked());

                if (holder.cbSelected.isChecked()) {
                    selectedContacts.add(holder.mItem);
                } else {
                    selectedContacts.remove(holder.mItem);
                }

                if (null != mListener) {
                    mListener.onSelect(selectedContacts.size());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredContacts != null ? filteredContacts.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mVisibleNameView;
        public final TextView mStatusTextView;
        public final CheckBox cbSelected;
        public final CircleImageView mProfileImageView;
        public Contact mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mVisibleNameView = view.findViewById(R.id.tv_visible_name);
            mStatusTextView = view.findViewById(R.id.tv_status_text);
            mProfileImageView = view.findViewById(R.id.iv_profile_image);
            cbSelected = view.findViewById(R.id.cb_selected);
        }
    }

    public interface ContactListListener {
        void onSelect(int count);
    }
}
