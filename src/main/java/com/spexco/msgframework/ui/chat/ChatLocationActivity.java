package com.spexco.msgframework.ui.chat;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.JsonObject;
import com.spexco.baselibrary.utils.FileUtils;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.utils.Utility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static com.spexco.msgframework.utils.Consts.KEY_LAT;
import static com.spexco.msgframework.utils.Consts.KEY_LNG;
import static com.spexco.msgframework.utils.Consts.LOCAL_FILE_DIR;
import static com.spexco.msgframework.utils.Consts.MESSAGE_TYPE_LOCATION;

public class ChatLocationActivity extends FragmentActivity implements OnMapReadyCallback {

    public static ChatActivity chatActivity;
    private GoogleMap map;
    private Marker marker;
    private TextView tvSend;
    private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_location);

        tvSend = findViewById(R.id.tv_send);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //TODO: bildirimde ve sohbetler listesinde kişi vb yazması
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        setMyLocation();

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                setLocation(latLng);
            }
        });
        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                setMyLocation();
                return false;
            }
        });
        map.setMyLocationEnabled(true);
    }

    private void setMyLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            setLocation(latLng);
                        }
                    }
                });
    }

    private void setLocation(LatLng latLng) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

        if (marker == null) {
            marker = map.addMarker(new MarkerOptions().position(latLng));
            tvSend.setEnabled(true);
            tvSend.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        } else {
            marker.setPosition(latLng);
        }
    }

    public void onBack(View view) {
        finish();
    }

    public void onSend(View view) {

        final ProgressDialog progress = ProgressDialog.show(ChatLocationActivity.this, "", "Gönderiliyor...");

        map.snapshot(new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap bitmap) {

                String filePath = LOCAL_FILE_DIR + "image.jpg";
                File file = new File(filePath);

                if (file.exists()) {
                    file.delete();
                }

                if (!file.exists()) {
                    try {
                        file.createNewFile();
                        FileUtils.writeBitmapToFile(bitmap, Bitmap.CompressFormat.JPEG, 80, filePath);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                Utility.uploadFile(new Utility.UploadFileListener() {
                    @Override
                    public void onResponse(boolean success, String description) {
                        if (success) {
                            LatLng latLng = marker.getPosition();
                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty(KEY_LAT, latLng.latitude);
                            jsonObject.addProperty(KEY_LNG, latLng.longitude);
                            String body = jsonObject.toString();
                            Message message = new Message(chatActivity.isGroupChat ? chatActivity.group.GroupID : chatActivity.conversation.ContactID, MESSAGE_TYPE_LOCATION, body, chatActivity.isGroupChat ? "" : chatActivity.conversation.ConversationID, chatActivity.isGroupChat);
                            message.MediaURLs = new ArrayList<>();
                            message.MediaURLs.add(description);
                            chatActivity.sendMessage(message);
                            progress.dismiss();
                            finish();
                        } else {
                            progress.dismiss();
                            Toast.makeText(getApplicationContext(), "Mesaj gönderilemedi", Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }
                }, file);
            }
        });


    }
}
