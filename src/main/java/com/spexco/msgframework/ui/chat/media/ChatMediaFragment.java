package com.spexco.msgframework.ui.chat.media;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.spexco.msgframework.R;

import java.io.File;

@SuppressLint("ValidFragment")
public class ChatMediaFragment extends Fragment {

    private ChatMediaPagerAdapter.MediaItem item;

    private ImageView imageView;
    private VideoView videoView;
    private MediaController mediaControls;

    public ChatMediaFragment(ChatMediaPagerAdapter.MediaItem item) {
        this.item = item;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_media_item, container, false);

        imageView = view.findViewById(R.id.iv_image);
        videoView = view.findViewById(R.id.vv_video);

        if (item.type == 1 || item.type == 3) {

            imageView.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(item.filePath).into(imageView);

        } else if (item.type == 2 || item.type == 4) {

            mediaControls = new MediaController(getActivity());
            videoView.setVisibility(View.VISIBLE);
            videoView.setMediaController(mediaControls);
            videoView.requestFocus();
            Uri uri;
            if (item.type == 4) {
                uri = Uri.parse(item.filePath);
            } else {
                File file = new File(item.filePath);
                uri = FileProvider.getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", file);
            }
            if (uri != null) {
                videoView.setVideoURI(uri);
                videoView.start();
            }
        }

        return view;
    }

}
