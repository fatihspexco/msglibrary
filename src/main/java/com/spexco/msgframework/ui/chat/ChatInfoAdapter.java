package com.spexco.msgframework.ui.chat;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Contact;
import com.spexco.msgframework.db.Participant;
import com.spexco.msgframework.utils.Consts;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.utils.Utility;

import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by fatihinan on 04.12.2018.
 */
public class ChatInfoAdapter extends RecyclerView.Adapter<ChatInfoAdapter.ViewHolder> {

    private List<Participant> participants;
    private int myRole;
    private ChatInfoActivity activity;

    public ChatInfoAdapter(ChatInfoActivity activity, List<Participant> participants, int role) {
        this.participants = participants;
        this.myRole = role;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_chat_info_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.participant = participants.get(position);
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return participants != null ? participants.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        Participant participant;

        final View mView;
        final TextView mRoleView;
        final TextView mVisibleNameView;
        final TextView mStatusTextView;
        final CircleImageView mProfileImageView;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mRoleView = view.findViewById(R.id.tv_role);
            mVisibleNameView = view.findViewById(R.id.tv_visible_name);
            mStatusTextView = view.findViewById(R.id.tv_status_text);
            mProfileImageView = view.findViewById(R.id.iv_profile_image);

            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showMenu();
                }
            });
        }

        void bind() {

            Contact contact = UserInfo.database.contactModel().getContactById(participant.UserID);

            if (participant.UserID.contentEquals(UserInfo.myUserID)) {
                mVisibleNameView.setText("Siz");
            } else {
                mVisibleNameView.setText(contact.VisibleName);
            }

            mStatusTextView.setText(contact.StatusText);

            if ( Utility.isValid(contact.PhotoURL) ) {
                Glide.with(UserInfo.context).load(contact.PhotoURL).into(mProfileImageView);
            } else {
                Glide.with(UserInfo.context).load(R.drawable.ic_default_user).into(mProfileImageView);
            }

            mRoleView.setText(Utility.getRoleText(participant.Role));
        }

        private void showMenu() {

            if (myRole != Consts.PARTICIPANT_ROLE_OWNER && myRole != Consts.PARTICIPANT_ROLE_ADMIN) {
                return;
            }

            if (participant.Role == Consts.PARTICIPANT_ROLE_OWNER) {
                return;
            }

            if (participant.UserID.contentEquals(UserInfo.myUserID)) {
                return;
            }

            PopupMenu popup = new PopupMenu(activity, mView);
            popup.inflate(R.menu.participant);
            Menu menu = popup.getMenu();

            if (participant.Role == Consts.PARTICIPANT_ROLE_ADMIN) {
                menu.findItem(R.id.participant_make_admin).setVisible(false);
            } else {
                menu.findItem(R.id.participant_remove_admin).setVisible(false);
            }

            if (myRole != Consts.PARTICIPANT_ROLE_OWNER && participant.Role == Consts.PARTICIPANT_ROLE_ADMIN) {
                menu.findItem(R.id.participant_remove).setVisible(false);
            }

            if (myRole != Consts.PARTICIPANT_ROLE_OWNER) {
                menu.findItem(R.id.participant_remove_admin).setVisible(false);
            }

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    int i = item.getItemId();
                    if (i == R.id.participant_remove) {
                        activity.onSelect(Collections.singletonList(participant.UserID), Consts.VALUE_TASK_GROUP_REMOVE_PARTICIPANT);
                        return true;
                    } else if (i == R.id.participant_make_admin) {
                        activity.makeAdmin(participant.UserID);
                        return true;
                    } else if (i == R.id.participant_remove_admin) {
                        activity.removeAdmin(participant.UserID);
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            popup.show();
        }
    }
}
