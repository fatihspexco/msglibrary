package com.spexco.msgframework.ui.chat.media;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.spexco.baselibrary.utils.FileUtils;
import com.spexco.baselibrary.videocompression.VideoCompressor;
import com.spexco.msgframework.R;
import com.spexco.msgframework.ui.chat.ChatActivity;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.utils.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.spexco.msgframework.utils.Consts.LOCAL_FILE_DIR;
import static com.spexco.msgframework.utils.Utility.dpToPx;

public class ChatMediaActivity extends FragmentActivity {

    //1: local image
    //2: local video
    //3: remote image
    //4: remote video
    public static int type;
    public static ChatActivity chatActivity;
    public static boolean isImageFromCamera = true;

    public static String text;
    public static ArrayList<String> filePaths;

    private RecyclerView rvPreview;
    private ChatMediaPreviewAdapter adapter;
    private ViewPager viewPager;

    private ImageView ivRemove;
    private LinearLayout chatbox;
    private EditText etMessage;
    private RelativeLayout buttons;
    private ImageButton btnSend;
    private ImageButton btnAdd;

    private ProgressDialog progress;

    private List<ChatMediaPagerAdapter.MediaItem> items;
    private ChatMediaPagerAdapter mPagerAdapter;
    private Message message;
    private int uploadCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_media);

        rvPreview = findViewById(R.id.rvPreview);
        rvPreview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        ivRemove = findViewById(R.id.iv_remove);
        chatbox = findViewById(R.id.layout_chatbox);
        rvPreview = findViewById(R.id.rvPreview);
        viewPager = findViewById(R.id.pager);

        items = new ArrayList<>();
        for (String fp : filePaths) {
            items.add(new ChatMediaPagerAdapter.MediaItem(fp, type));
        }

        adapter = new ChatMediaPreviewAdapter(items, this);
        rvPreview.setAdapter(adapter);

        mPagerAdapter = new ChatMediaPagerAdapter(getSupportFragmentManager(), items);
        viewPager.setAdapter(mPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) { }
            @Override
            public void onPageSelected(int i) {
                setPreview(i);
            }
            @Override
            public void onPageScrollStateChanged(int i) { }
        });

        if (type == 1 || type == 2) {
            chatbox.setVisibility(View.VISIBLE);
            etMessage = findViewById(R.id.et_message);

            buttons = findViewById(R.id.layout_buttons);
            buttons.setVisibility(View.VISIBLE);
            btnSend = findViewById(R.id.btn_send);
            btnAdd = findViewById(R.id.btn_add);

            if (type == 1) {
                btnAdd.setVisibility(View.VISIBLE);
                btnAdd.setOnClickListener(addClickListener);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) etMessage.getLayoutParams();
                layoutParams.setMarginStart(dpToPx(48));
                etMessage.setLayoutParams(layoutParams);
            }

            btnSend.setOnClickListener(sendClickListener);

            etMessage.setText(text);

            ivRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRemove();
                }
            });

        } else {
            rvPreview.setVisibility(items.size() > 1 ? View.VISIBLE : View.GONE);
            chatbox.setVisibility(View.GONE);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) rvPreview.getLayoutParams();
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            rvPreview.setLayoutParams(layoutParams);
        }
    }

    private View.OnClickListener addClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (type != 1) {
                return;
            }
            if (isImageFromCamera) {
                onPhoto();
            } else {
                onGallery();
            }
        }
    };

    public void setAdapter(int index) {
        boolean removeEnabled = type <=2 && items.size() > 1;
        ivRemove.setVisibility(removeEnabled ? View.VISIBLE : View.GONE);
        viewPager.setCurrentItem(index);

        rvPreview.setVisibility(items.size() > 1 ? View.VISIBLE : View.GONE);
        setPreview(index);
    }

    private void setPreview(final int index) {
        adapter.selectedIndex = index;
        adapter.notifyDataSetChanged();
        rvPreview.smoothScrollToPosition(index);
    }

    public void onRemove() {
        int index = viewPager.getCurrentItem();
        items.remove(index);
        mPagerAdapter = new ChatMediaPagerAdapter(getSupportFragmentManager(), items);
        viewPager.setAdapter(mPagerAdapter);

        adapter = new ChatMediaPreviewAdapter(items, this);
        adapter.selectedIndex = index - 1;
        rvPreview.setAdapter(adapter);

        setAdapter(index - 1);
    }

    private void onPhoto() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                File file = new File(LOCAL_FILE_DIR + "image.jpg");
                Uri uri = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".provider", file);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(takePictureIntent, ChatActivity.REQUEST_IMAGE_CAPTURE);
            } else {
                Toast.makeText(getApplicationContext(), "Cihazınız fotoğraf çekimini desteklemiyor.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onGallery() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Seçiniz"), ChatActivity.REQUEST_GALLERY);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[] {"image/*"});
            startActivityForResult(intent, ChatActivity.REQUEST_GALLERY);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == RESULT_OK) {

                if (requestCode == ChatActivity.REQUEST_IMAGE_CAPTURE) {

                    String targetDir = LOCAL_FILE_DIR;
                    String fileName = UUID.randomUUID().toString();

                    File file = new File(LOCAL_FILE_DIR + "image.jpg");
                    FileUtils.writeJpgFile(file, targetDir, fileName);
                    String filePath = targetDir + fileName + ".jpg";
                    items.add(new ChatMediaPagerAdapter.MediaItem(filePath, type));
                    mPagerAdapter.notifyDataSetChanged();
                    setAdapter(items.size() - 1);

                } else if (requestCode == ChatActivity.REQUEST_GALLERY) {

                    if (data != null) {
                        if (data.getData() != null){
                            Uri uri = data.getData();

                            String filePath = FileUtils.getPath(getApplicationContext(), uri);
                            items.add(new ChatMediaPagerAdapter.MediaItem(filePath, type));
                            mPagerAdapter.notifyDataSetChanged();
                            setAdapter(items.size() - 1);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener sendClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (type > 2) {
                return;
            }

            if (progress == null) {
                progress = ProgressDialog.show(ChatMediaActivity.this, "", "Gönderiliyor...");
            } else {
                progress.show();
            }

            if (type == 2) {
                final String input = LOCAL_FILE_DIR + "video.mp4";
                final String output = LOCAL_FILE_DIR + "video_compressed.mp4";
                new VideoCompressor(input, output, new VideoCompressor.VideoCompressListener() {
                    @Override
                    public void onCompress(boolean success) {
                        File file = new File(input);
                        if (success) {
                            file = new File(output);
                        }
                        uploadFile(file);
                    }
                }).execute();
            } else {
                for (ChatMediaPagerAdapter.MediaItem item : items) {
                    File file = new File(item.filePath);
                    uploadFile(file);
                }
            }
        }
    };

    private void uploadFile(File file) {
        Utility.uploadFile(new Utility.UploadFileListener() {
            @Override
            public void onResponse(boolean success, String description) {
                if (success) {

                    if (message == null) {
                        String text = etMessage.getText().toString();
                        etMessage.setText("");
                        message = new Message(chatActivity.isGroupChat ? chatActivity.group.GroupID : chatActivity.conversation.ContactID, type, text, chatActivity.isGroupChat ? "" : chatActivity.conversation.ConversationID, chatActivity.isGroupChat);
                        message.MediaURLs = new ArrayList<>();
                    }

                    message.MediaURLs.add(description);
                    uploadCount++;

                    if (uploadCount == items.size()) {
                        chatActivity.sendMessage(message);
                        chatActivity.etMessage.setText("");
                        progress.dismiss();
                        finish();
                    }
                } else {
                    progress.dismiss();
                    Toast.makeText(getApplicationContext(), "Mesaj gönderilemedi", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }, file);
    }
}
