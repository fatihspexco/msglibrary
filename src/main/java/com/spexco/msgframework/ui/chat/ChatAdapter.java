package com.spexco.msgframework.ui.chat;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.spexco.baselibrary.utils.AlertUtil;
import com.spexco.baselibrary.utils.DeviceUtils;
import com.spexco.baselibrary.utils.FileUtils;
import com.spexco.msgframework.MessageApplication;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.db.MessageStatus;
import com.spexco.msgframework.socket.BaseParser;
import com.spexco.msgframework.ui.chat.media.ChatMediaActivity;
import com.spexco.msgframework.utils.Consts;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.Context.CLIPBOARD_SERVICE;

public class ChatAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_MESSAGE_INFO = 0;
    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private RequestOptions videoRequestOptions;

    private final ChatActivity activity;

    public static int leftImageDp;

    private Context mContext;
    private List<Message> mMessageList;

    public ChatAdapter(Context context, ChatActivity activity, List<Message> messages) {
        mContext = context;
        mMessageList = messages;
        leftImageDp = DeviceUtils.convertDpToPixels(mContext, 50);
        videoRequestOptions = new RequestOptions().override(500, 500);
        this.activity = activity;
    }

    public void setMessages(List<Message> messages) {
        mMessageList = messages;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mMessageList != null ? mMessageList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        Message message = mMessageList.get(position);

        if (message.MessageType == Consts.MESSAGE_TYPE_INFO) {
            return VIEW_TYPE_MESSAGE_INFO;
        } else if (message.From.contentEquals(UserInfo.myUserID)) {
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_INFO) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message_type_info, parent, false);
            return new InfoMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message_outgoing, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message_incoming, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        Message message = mMessageList.get(position);

        if (viewHolder.getItemViewType() == VIEW_TYPE_MESSAGE_INFO) {
            InfoMessageHolder holder = (InfoMessageHolder) viewHolder;
            holder.bind(message);
            return;
        }

        BaseMessageHolder holder = (BaseMessageHolder) viewHolder;
        holder.message = message;
        if (position == mMessageList.size() - 1) {
            Log.e("testtest", "loadmore");
            activity.onLoadMore(message);
        }

        //Log.e("testtest", "onBindViewHolder - " + position + " " + message.Body);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind();
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:

                boolean showName = message.isGroup != null && message.isGroup && position == mMessageList.size() - 1;
                if (position < mMessageList.size() - 1) {
                    showName = message.isGroup != null && message.isGroup && !mMessageList.get(position + 1).From.contentEquals(message.From);
                }

                ((ReceivedMessageHolder) holder).bind(showName);
                break;
        }
    }

    public class InfoMessageHolder extends RecyclerView.ViewHolder {

        TextView tvBody;

        public InfoMessageHolder(@NonNull View itemView) {
            super(itemView);
            tvBody = itemView.findViewById(R.id.tv_body);
        }

        public void bind(Message message) {
            tvBody.setText(Utility.getInfoMessageText(message.Body));
        }
    }

    public class BaseMessageHolder extends RecyclerView.ViewHolder {

        Message message;

        RelativeLayout rlImage;
        LinearLayout llMulipleImages, llMulipleImagesSecondRow;
        ImageView ivImage1, ivImage2, ivImage3, ivImage4;
        TextView tvImageMore;

        TextView tvBody;
        TextView tvTime;
        RelativeLayout rlAllView;
        ImageView imageView;
        ImageView ivVideo;

        public BaseMessageHolder(@NonNull View itemView) {
            super(itemView);

            rlImage = itemView.findViewById(R.id.rl_image);
            llMulipleImages = itemView.findViewById(R.id.ll_multiple_images);
            llMulipleImagesSecondRow = itemView.findViewById(R.id.ll_multiple_images_second_row);
            ivImage1 = itemView.findViewById(R.id.iv_image_1);
            ivImage2 = itemView.findViewById(R.id.iv_image_2);
            ivImage3 = itemView.findViewById(R.id.iv_image_3);
            ivImage4 = itemView.findViewById(R.id.iv_image_4);
            tvImageMore = itemView.findViewById(R.id.tv_image_more);

            imageView = itemView.findViewById(R.id.iv_image);
            ivVideo = itemView.findViewById(R.id.iv_image_video);
            tvBody = itemView.findViewById(R.id.tv_body);
            tvTime = itemView.findViewById(R.id.tv_time);
            rlAllView = itemView.findViewById(R.id.rl_all_view);
            rlAllView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (message.MessageType != Consts.MESSAGE_TYPE_OTHER &&
                            message.MessageType != Consts.MESSAGE_TYPE_PICTURE &&
                            message.MessageType != Consts.MESSAGE_TYPE_VIDEO &&
                            message.MessageType != Consts.MESSAGE_TYPE_LOCATION &&
                            message.MessageType != Consts.MESSAGE_TYPE_CONTACT) {
                        return;
                    }

                    if (message.MessageType == Consts.MESSAGE_TYPE_CONTACT) {
                        mContext.startActivity(Utility.getContactIntent(message.Body));
                        return;
                    }

                    if (message.MediaURLs == null || message.MediaURLs.size() == 0) {
                        return;
                    }

                    if (message.MessageType == Consts.MESSAGE_TYPE_LOCATION) {
                        try {
                            JSONObject jsonObject = new JSONObject(message.Body);
                            String lat = jsonObject.getString(Consts.KEY_LAT);
                            String lng = jsonObject.getString(Consts.KEY_LNG);
                            String uri = String.format(Locale.ENGLISH, "geo:%s,%s?q=%s,%s", lat, lng, lat, lng);
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                            mContext.startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return;
                    }

                    if (message.MessageType == Consts.MESSAGE_TYPE_OTHER) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(message.MediaURLs.get(0)));
                        //intent.setData(Uri.parse( "http://docs.google.com/viewer?url=" + message.MediaURLs.get(0)), "text/html");
                        mContext.startActivity(intent);
                    } else {
                        ChatMediaActivity.type = message.MessageType == Consts.MESSAGE_TYPE_PICTURE ? 3 : 4;
                        ChatMediaActivity.filePaths = message.MediaURLs;
                        ChatMediaActivity.chatActivity = activity;
                        ChatMediaActivity.isImageFromCamera = false;
                        Intent intent = new Intent(mContext, ChatMediaActivity.class);
                        mContext.startActivity(intent);
                    }

                }
            });
            rlAllView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    showMenu();
                    return false;
                }
            });
        }

        public void showMenu() {

            PopupMenu popup = new PopupMenu(mContext, rlAllView);
            popup.inflate(R.menu.message);


            final Menu menu = popup.getMenu();
            menu.findItem(R.id.message_info).setVisible(message.From.contentEquals(UserInfo.myUserID));
            menu.findItem(R.id.message_delete).setVisible(message.From.contentEquals(UserInfo.myUserID));
            menu.findItem(R.id.message_copy).setVisible(message.MessageType == Consts.MESSAGE_TYPE_TEXT);

            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    int i = item.getItemId();
                    if (i == R.id.message_info) {
                        activity.onInfo(message);
                        return true;
                    } else if (i == R.id.message_forward) {
                        activity.onForward(message);
                        return true;
                    } else if (i == R.id.message_copy) {
                        ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("spec", message.Body);
                        clipboard.setPrimaryClip(clip);
                        Toast.makeText(activity, "Mesaj panoya kopyalandı", Toast.LENGTH_SHORT).show();
                        return true;
                    } else if (i == R.id.message_delete) {
                        showDeleteMenu();
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            popup.show();
        }

        private void showDeleteMenu() {
            AlertUtil.showAlert(mContext, false,
                    "Bu mesajı silmek istediğinizden emin misiniz?", null,
                    "Herkesten Sil", null,
                    "Benden Sil", null,
                    "Vazgeç", null);
        }

        void bind() {

            if ((message.MessageType == Consts.MESSAGE_TYPE_PICTURE || message.MessageType == Consts.MESSAGE_TYPE_VIDEO || message.MessageType == Consts.MESSAGE_TYPE_LOCATION) &&
                    (message.MediaURLs != null && message.MediaURLs.size() > 0)) {

                llMulipleImagesSecondRow.setVisibility(View.GONE);
                tvImageMore.setVisibility(View.GONE);
                rlImage.setVisibility(View.VISIBLE);

                if (message.MediaURLs.size() > 1) {
                    imageView.setVisibility(View.GONE);
                    ivVideo.setVisibility(View.GONE);
                    llMulipleImages.setVisibility(View.VISIBLE);
                    if (message.MediaURLs.size() == 2) {
                        Glide.with(mContext).load(message.MediaURLs.get(0)).into(ivImage1);
                        Glide.with(mContext).load(message.MediaURLs.get(1)).into(ivImage2);
                    } else if (message.MediaURLs.size() == 3) {
                        Glide.with(mContext).load(message.MediaURLs.get(0)).into(ivImage1);
                        Glide.with(mContext).load(message.MediaURLs.get(1)).into(ivImage2);
                        Glide.with(mContext).load(message.MediaURLs.get(2)).into(ivImage3);
                        llMulipleImagesSecondRow.setVisibility(View.VISIBLE);
                    } else if (message.MediaURLs.size() == 4) {
                        Glide.with(mContext).load(message.MediaURLs.get(0)).into(ivImage1);
                        Glide.with(mContext).load(message.MediaURLs.get(1)).into(ivImage2);
                        Glide.with(mContext).load(message.MediaURLs.get(2)).into(ivImage3);
                        Glide.with(mContext).load(message.MediaURLs.get(3)).into(ivImage4);
                        llMulipleImagesSecondRow.setVisibility(View.VISIBLE);
                    } else {
                        Glide.with(mContext).load(message.MediaURLs.get(0)).into(ivImage1);
                        Glide.with(mContext).load(message.MediaURLs.get(1)).into(ivImage2);
                        Glide.with(mContext).load(message.MediaURLs.get(2)).into(ivImage3);
                        Glide.with(mContext).load(message.MediaURLs.get(3)).into(ivImage4);
                        llMulipleImagesSecondRow.setVisibility(View.VISIBLE);
                        tvImageMore.setVisibility(View.VISIBLE);
                        tvImageMore.setText("+" + (message.MediaURLs.size() - 3));
                    }

                } else {
                    llMulipleImages.setVisibility(View.GONE);
                    imageView.setVisibility(View.VISIBLE);
                    if (message.MessageType == Consts.MESSAGE_TYPE_VIDEO) {
                        Glide.with(mContext).load(message.MediaURLs.get(0)).apply(videoRequestOptions).into(imageView);
                        ivVideo.setVisibility(View.VISIBLE);
                    } else {
                        Glide.with(mContext).load(message.MediaURLs.get(0)).into(imageView);
                        ivVideo.setVisibility(View.GONE);
                    }
                }
            } else {
                rlImage.setVisibility(View.GONE);
            }

            if (message.MessageType == Consts.MESSAGE_TYPE_OTHER) {
                Drawable img = ContextCompat.getDrawable(mContext, FileUtils.getMimetypeImage(message.Body));
                img.setBounds(0, 0, leftImageDp, leftImageDp);
                tvBody.setCompoundDrawables(img, null, null, null);
            } else if (message.MessageType == Consts.MESSAGE_TYPE_CONTACT) {
                Drawable img = ContextCompat.getDrawable(mContext, R.drawable.default_user);
                img.setBounds(0, 0, leftImageDp, leftImageDp);
                tvBody.setCompoundDrawables(img, null, null, null);

            } else {
                tvBody.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }

            if (message.MessageType == Consts.MESSAGE_TYPE_CONTACT) {
                tvBody.setVisibility(View.VISIBLE);
                tvBody.setText(Utility.getContactInfo(message.Body));
            } else if (message.MessageType != Consts.MESSAGE_TYPE_LOCATION && message.Body != null && message.Body.length() > 0) {
                tvBody.setVisibility(View.VISIBLE);
                tvBody.setText(message.Body);
            } else {
                tvBody.setVisibility(View.GONE);
                tvBody.setText("");
            }

            tvTime.setText(Utility.convertDateToString(message.CreateDate, "HH:mm"));
        }
    }

    private class SentMessageHolder extends BaseMessageHolder implements BaseParser.UpdateListener {

        ImageView ivMessageStatus;

        SentMessageHolder(View itemView) {
            super(itemView);

            BaseParser.addListener(this);
            ivMessageStatus = itemView.findViewById(R.id.iv_message_status);
        }

        void bind() {

            super.bind();

            if (message.isGroup != null && message.isGroup) {
                onUpdate(BaseParser.UpdateType.MESSAGE_STATUS);
            } else {
                ivMessageStatus.setImageResource(message.getMessageStatusResource());
            }
        }

        @Override
        public void onUpdate(BaseParser.UpdateType type) {
            if (type == BaseParser.UpdateType.MESSAGE_STATUS) {
                MessageApplication.mainHandler.post(updateRunnable);
            }
        }

        private Runnable updateRunnable = new Runnable() {
            @Override
            public void run() {
                List<MessageStatus> messageStatuses = UserInfo.database.messageStatusModel().getMessageStatus(message.MessageID);

                if (getAdapterPosition() < 0) {
                    return;
                }

                int unseenCount = 0;
                int undeliveredCount = 0;
                for (MessageStatus m : messageStatuses) {
                    if (m.Status != Consts.MESSAGE_STATUS_SEEN) {
                        unseenCount++;
                    }
                    if (m.Status != Consts.MESSAGE_STATUS_DELIVERED && m.Status != Consts.MESSAGE_STATUS_SEEN) {
                        undeliveredCount++;
                    }
                }
                if (unseenCount == 0) {
                    ivMessageStatus.setImageResource(R.drawable.ic_message_status_seen);
                } else if (undeliveredCount == 0) {
                    ivMessageStatus.setImageResource(R.drawable.ic_message_status_delivered);
                } else if (message.Status >= Consts.MESSAGE_STATUS_SEND) {
                    ivMessageStatus.setImageResource(R.drawable.ic_message_status_sent);
                } else {
                    ivMessageStatus.setImageResource(R.drawable.ic_message_status_waiting);
                }
            }
        };

        @Override
        public String getUniqueID() {
            return message.MessageID;
        }
    }

    private class ReceivedMessageHolder extends BaseMessageHolder {

        TextView tvVisibleName;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            tvVisibleName = itemView.findViewById(R.id.tv_visible_name);
        }

        void bind(boolean showName) {

            super.bind();

            if (showName) {
                tvVisibleName.setVisibility(View.VISIBLE);
                tvVisibleName.setText(UserInfo.database.contactModel().getContactVisibleName(message.From));
            } else {
                tvVisibleName.setVisibility(View.GONE);
            }

            if ( !message.From.contentEquals(UserInfo.myUserID) && message.Status < Consts.MESSAGE_STATUS_SEEN) {
                message.Status = Consts.MESSAGE_STATUS_SEEN;
                message.SeenDate = new Date();

                UserInfo.database.messageModel().insertMessage(message);
                Utility.updateMessage(message);
            }
        }
    }

    interface MessageListener {

        void onInfo(Message message);

        void onLoadMore(Message message);

        void onForward(Message message);
    }
}