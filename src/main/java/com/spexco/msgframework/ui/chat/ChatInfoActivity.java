package com.spexco.msgframework.ui.chat;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.spexco.baselibrary.utils.AlertUtil;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Conversation;
import com.spexco.msgframework.db.Group;
import com.spexco.msgframework.db.Participant;
import com.spexco.msgframework.ui.creategroup.participant.CreateGroupActivity;
import com.spexco.msgframework.utils.Consts;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.utils.Utility;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChatInfoActivity extends AppCompatActivity implements CreateGroupActivity.SelectParticipantListener {

    public static boolean isGroup = false;
    public static String uniqueID;

    private boolean isFirst = true;

    private int myRole;

    private Conversation conversation;
    private Group group;
    private List<Participant> participants;

    private LinearLayout llParticipant;
    private RelativeLayout rlMessageAuth;
    private TextView tvMessageAuth;
    private TextView tvParticipantCount;
    private TextView tvAdd;
    private RecyclerView rvParticipant;

    private ImageView ivProfile;
    private ImageButton ivSelectProfile;
    private TextView tvVisibleName;
    private TextView tvStatusText;

    private ProgressDialog progress;
    private Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_info);

        ((TextView) findViewById(R.id.tv_title)).setText(isGroup ? "Grup Bilgisi" : "Kişi Bilgisi");

        ivProfile = findViewById(R.id.iv_profile_image);
        ivSelectProfile = findViewById(R.id.iv_select_profile);
        tvVisibleName = findViewById(R.id.tv_visible_name);
        tvStatusText = findViewById(R.id.tv_status_text);

        llParticipant = findViewById(R.id.ll_participant);
        rlMessageAuth = findViewById(R.id.rlMessageAuth);
        tvMessageAuth = findViewById(R.id.tvMessageAuth);
        tvParticipantCount = findViewById(R.id.tv_participant_count);
        tvAdd = findViewById(R.id.tv_add);
        rvParticipant = findViewById(R.id.rv_participant);
        rvParticipant.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        if (isGroup) {
            loadGroup();
        } else {
            llParticipant.setVisibility(View.GONE);
            loadConversation();
        }

    }

    private void loadConversation() {
        UserInfo.database.conversationModel().getConversationWithInfo(uniqueID).observe(this, new Observer<Conversation>() {
            @Override
            public void onChanged(@Nullable Conversation conversationWithInfo) {

                conversation = conversationWithInfo;

                if (conversation != null) {

                    if ( Utility.isValid(conversation.PhotoURL) ) {
                        Glide.with(UserInfo.context).load(conversation.PhotoURL).into(ivProfile);
                    } else {
                        Glide.with(UserInfo.context).load(R.drawable.ic_default_user).into(ivProfile);
                    }
                    tvVisibleName.setText(conversation.VisibleName);
                    tvStatusText.setText(UserInfo.database.contactModel().getContactStatusText(conversation.ContactID));
                }
            }
        });
    }

    private void loadGroup() {
        UserInfo.database.groupModel().getGroupWithInfo(uniqueID).observe(this, new Observer<Group>() {
            @Override
            public void onChanged(@Nullable Group groupWithInfo) {

                group = groupWithInfo;

                if (group != null) {

                    if ( Utility.isValid(group.PhotoURL) ) {
                        Glide.with(UserInfo.context).load(group.PhotoURL).into(ivProfile);
                    } else {
                        Glide.with(UserInfo.context).load(R.drawable.ic_default_group).into(ivProfile);
                    }
                    tvVisibleName.setText(group.GroupName);
                    tvMessageAuth.setText(group.MessageAuth == 0 ? "Sadece Yöneticiler" : "Bütün Katılımcılar");

                    if ( Utility.isValid(group.GroupDescription) ) {
                        tvStatusText.setText(group.GroupDescription);
                    } else {
                        tvStatusText.setText("Grup açıklaması ekleyin");
                    }

                    if (isFirst) {
                        isFirst = false;
                        UserInfo.database.participantModel().getLiveParticipants(group.GroupID).observe(ChatInfoActivity.this, new Observer<List<Participant>>() {
                            @Override
                            public void onChanged(@Nullable List<Participant> participants) {

                                if (participants != null) {

                                    ChatInfoActivity.this.participants = participants;

                                    boolean isDeleted = true;
                                    for (Participant p : participants) {
                                        if (p.UserID.contentEquals(UserInfo.myUserID)) {
                                            setMyRole(p.Role);
                                            isDeleted = false;
                                            break;
                                        }
                                    }
                                    if (isDeleted) {
                                        setMyRole(Consts.PARTICIPANT_ROLE_DELETED);
                                    }

                                    tvParticipantCount.setText(participants.size() + " KATILIMCI");
                                    ChatInfoAdapter adapter = new ChatInfoAdapter(ChatInfoActivity.this, participants, myRole);
                                    rvParticipant.setAdapter(adapter);
                                }
                            }
                        });
                    }

                }
            }
        });

    }

    public void onProfileSelect(View view) {
        CropImage.activity().setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                fileUri = result.getUri();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        progress = ProgressDialog.show(ChatInfoActivity.this, "", "Kaydediliyor...");
                        Utility.uploadFile(new Utility.UploadFileListener() {
                            @Override
                            public void onResponse(boolean success, String description) {
                                if (success) {
                                    UserInfo.updateGroup(group.GroupID, description, group.GroupName, group.GroupDescription, group.MessageAuth);
                                }
                                fileUri = null;
                                progress.dismiss();
                            }
                        }, new File(fileUri.getPath()));

                    }
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void setMyRole(int role) {
        myRole = role;

        if (myRole == Consts.PARTICIPANT_ROLE_DELETED) {
            finish();
        }

        if (myRole == Consts.PARTICIPANT_ROLE_OWNER || myRole == Consts.PARTICIPANT_ROLE_ADMIN) {
            tvAdd.setVisibility(View.VISIBLE);
            ivSelectProfile.setVisibility(View.VISIBLE);

            tvVisibleName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChatInfoActivity.this);
                    alertDialog.setTitle("Grup Adı");

                    final EditText input = new EditText(ChatInfoActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    input.setText(group.GroupName);
                    alertDialog.setView(input);

                    alertDialog.setPositiveButton("Kaydet",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String groupName = input.getText().toString();
                                    if (groupName.trim().length() == 0) {
                                        Toast.makeText(getApplicationContext(), "Lütfen grup adı giriniz.", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    UserInfo.updateGroup(group.GroupID, "", groupName, group.GroupDescription, group.MessageAuth);
                                }
                            });

                    alertDialog.setNegativeButton("Vazgeç", null);
                    alertDialog.show();
                }
            });

            tvStatusText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChatInfoActivity.this);
                    alertDialog.setTitle("Grup Açıklaması");

                    final EditText input = new EditText(ChatInfoActivity.this);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    input.setLayoutParams(lp);
                    input.setText(group.GroupDescription);
                    alertDialog.setView(input);

                    alertDialog.setPositiveButton("Kaydet",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    String groupDesc = input.getText().toString();
                                    if (groupDesc.trim().length() == 0) {
                                        Toast.makeText(getApplicationContext(), "Lütfen grup açıklaması giriniz.", Toast.LENGTH_SHORT).show();
                                        return;
                                    }
                                    UserInfo.updateGroup(group.GroupID, "", group.GroupName, groupDesc, group.MessageAuth);
                                }
                            });

                    alertDialog.setNegativeButton("Vazgeç", null);
                    alertDialog.show();
                }
            });

            rlMessageAuth.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu popup = new PopupMenu(ChatInfoActivity.this, tvMessageAuth);
                    popup.inflate(R.menu.message_auth);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int i = item.getItemId();
                            if (i == R.id.message_auth_all_participants) {
                                UserInfo.updateGroup(group.GroupID, "", group.GroupName, group.GroupDescription, 1);
                                return true;
                            } else if (i == R.id.message_auth_only_admins) {
                                UserInfo.updateGroup(group.GroupID, "", group.GroupName, group.GroupDescription, 0);
                                return true;
                            } else {
                                return false;
                            }
                        }
                    });
                    popup.show();
                }
            });
        } else {
            tvAdd.setVisibility(View.GONE);
            ivSelectProfile.setVisibility(View.GONE);
            tvVisibleName.setOnClickListener(null);
            tvStatusText.setOnClickListener(null);
            rlMessageAuth.setOnClickListener(null);
        }
    }

    public void onBack(View view) {
        finish();
    }

    public void onLeave(View view) {

        AlertUtil.showAlert(ChatInfoActivity.this, "Gruptan Çık", "Gruptan çıkmak istediğinizden emin misiniz?",
                "Evet", leave,
                "İptal", null);

    }

    private DialogInterface.OnClickListener leave = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            onSelect(Collections.singletonList(UserInfo.myUserID), Consts.VALUE_TASK_GROUP_REMOVE_PARTICIPANT);
            finish();
        }
    };

    public void onAdd(View view) {

        Intent intent = new Intent(ChatInfoActivity.this, CreateGroupActivity.class);
        CreateGroupActivity.listener = this;

        List<String> selectedContacts = new ArrayList<>();
        if (participants != null) {
            for (Participant p : participants) {
                selectedContacts.add(p.UserID);
            }
        }
        CreateGroupActivity.selectedContacts = selectedContacts;
        startActivity(intent);
    }

    public void makeAdmin(String userID) {
        addRemoveAdmin(userID, Consts.VALUE_TASK_MAKE_ADMIN);
    }

    public void removeAdmin(String userID) {
        addRemoveAdmin(userID, Consts.VALUE_TASK_REMOVE_ADMIN);
    }

    private void addRemoveAdmin(String userID, String task) {

        JsonObject messageObject = new JsonObject();
        messageObject.addProperty(Consts.KEY_TYPE, Consts.VALUE_TYPE_SET);
        messageObject.addProperty(Consts.KEY_TASK, task);
        messageObject.addProperty(Consts.KEY_FROM, UserInfo.myUserID);
        messageObject.addProperty(Consts.KEY_GROUP_ID, group.GroupID);
        messageObject.addProperty(Consts.KEY_USER_ID, userID);

        UserInfo.sendToSocket(messageObject.toString());
    }

    @Override
    public void onSelect(List<String> userIDs, String task) {

        JsonObject messageObject = new JsonObject();
        messageObject.addProperty(Consts.KEY_TYPE, Consts.VALUE_TYPE_SET);
        messageObject.addProperty(Consts.KEY_TASK, task);
        messageObject.addProperty(Consts.KEY_FROM, UserInfo.myUserID);
        messageObject.addProperty(Consts.KEY_GROUP_ID, group.GroupID);

        JsonArray participantsArray = new JsonArray();
        for (String userID : userIDs) {
            participantsArray.add(userID);
        }
        messageObject.add(Consts.KEY_PARTICIPANTS, participantsArray);

        UserInfo.sendToSocket(messageObject.toString());
    }


}
