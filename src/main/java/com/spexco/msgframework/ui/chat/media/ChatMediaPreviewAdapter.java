package com.spexco.msgframework.ui.chat.media;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.spexco.msgframework.R;

import java.util.List;

/**
 * Created by fatihinan on 04.12.2018.
 */
public class ChatMediaPreviewAdapter extends RecyclerView.Adapter<ChatMediaPreviewAdapter.ViewHolder> {

    public int selectedIndex = 0;
    private ChatMediaActivity activity;
    private List<ChatMediaPagerAdapter.MediaItem> items;

    public ChatMediaPreviewAdapter(List<ChatMediaPagerAdapter.MediaItem> items, ChatMediaActivity activity) {
        this.items = items;
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_media_preview, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Glide.with(holder.ivPreview).load(items.get(position).filePath).into(holder.ivPreview);

        holder.setSelected(position == selectedIndex);

        holder.ivPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setAdapter(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivPreview;

        public ViewHolder(View view) {
            super(view);
            ivPreview = view.findViewById(R.id.ivPreview);
        }

        public void setSelected(boolean selected) {
            ivPreview.setBackgroundResource(selected ? R.drawable.preview_selected : 0);
        }
    }
}
