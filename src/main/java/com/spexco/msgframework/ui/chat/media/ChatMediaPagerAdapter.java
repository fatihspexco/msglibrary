package com.spexco.msgframework.ui.chat.media;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

public class ChatMediaPagerAdapter extends FragmentStatePagerAdapter {

    List<MediaItem> items;

    public ChatMediaPagerAdapter(FragmentManager fm, List<MediaItem> items) {
        super(fm);
        this.items = items;
    }

    @Override
    public Fragment getItem(int position) {
        return new ChatMediaFragment(items.get(position));
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public static class MediaItem {
        public String filePath;
        public int type;

        public MediaItem(String filePath, int type) {
            this.filePath = filePath;
            this.type = type;
        }
    }
}