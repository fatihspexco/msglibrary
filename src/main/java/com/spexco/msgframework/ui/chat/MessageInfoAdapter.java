package com.spexco.msgframework.ui.chat;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Contact;
import com.spexco.msgframework.db.MessageStatus;

import java.util.List;

import static com.spexco.msgframework.utils.Consts.MESSAGE_STATUS_DELIVERED;
import static com.spexco.msgframework.utils.Consts.MESSAGE_STATUS_SEEN;
import static com.spexco.msgframework.utils.UserInfo.database;
import static com.spexco.msgframework.utils.Utility.getTimeAgo;
import static com.spexco.msgframework.utils.Utility.isValid;

public class MessageInfoAdapter extends RecyclerView.Adapter<MessageInfoAdapter.ViewHolder> {

    private List<MessageStatus> messageStatuses;
    private List<String> dateList;
    private boolean isGroup;

    public MessageInfoAdapter(List<MessageStatus> messageStatuses, List<String> dateList, boolean isGroup) {
        this.messageStatuses = messageStatuses;
        this.dateList = dateList;
        this.isGroup = isGroup;
    }

    @Override
    public int getItemCount() {
        return isGroup ? messageStatuses.size() : dateList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (isGroup) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message_info, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message_info_one, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (isGroup) {
            MessageStatus messageStatus = messageStatuses.get(position);
            holder.bind(messageStatus);
        } else {

            holder.mTimeView.setText(dateList.get(position));

            if (position == 0) {

                holder.mProfileImageView.setImageResource(R.drawable.ic_message_status_seen);
                holder.mVisibleNameView.setText("Okundu");

            } else if (position == 1) {

                holder.mProfileImageView.setImageResource(R.drawable.ic_message_status_delivered);
                holder.mVisibleNameView.setText("Teslim edildi");
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView mVisibleNameView;
        TextView mTimeView;
        ImageView mProfileImageView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            mVisibleNameView = itemView.findViewById(R.id.tv_visible_name);
            mTimeView = itemView.findViewById(R.id.tv_time);
            mProfileImageView = itemView.findViewById(R.id.iv_profile_image);
        }

        void bind(MessageStatus messageStatus) {

            Contact contact = database.contactModel().getContactById(messageStatus.UserID);
            mVisibleNameView.setText(contact.VisibleName);

            if (messageStatus.Status == MESSAGE_STATUS_SEEN) {
                mTimeView.setText(getTimeAgo(messageStatus.SeenDate));
            } else if (messageStatus.Status == MESSAGE_STATUS_DELIVERED) {
                mTimeView.setText(getTimeAgo(messageStatus.DeliverDate));
            }

            if ( isValid(contact.PhotoURL) ) {
                Glide
                    .with(mProfileImageView)
                    .load(contact.PhotoURL)
                    .into(mProfileImageView);
            } else {
                Glide
                    .with(mProfileImageView)
                    .load(R.drawable.ic_default_user)
                    .into(mProfileImageView);
            }
        }
    }

}