package com.spexco.msgframework.ui.chat;

import android.arch.lifecycle.Observer;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spexco.baselibrary.utils.FileUtils;
import com.spexco.msgframework.MessageApplication;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.db.MessageStatus;
import com.spexco.msgframework.socket.BaseParser;

import java.util.ArrayList;
import java.util.List;

import static com.spexco.msgframework.ui.chat.ChatAdapter.leftImageDp;
import static com.spexco.msgframework.utils.Consts.MESSAGE_STATUS_DELIVERED;
import static com.spexco.msgframework.utils.Consts.MESSAGE_STATUS_SEEN;
import static com.spexco.msgframework.utils.Consts.MESSAGE_STATUS_SEND;
import static com.spexco.msgframework.utils.Consts.MESSAGE_TYPE_CONTACT;
import static com.spexco.msgframework.utils.Consts.MESSAGE_TYPE_LOCATION;
import static com.spexco.msgframework.utils.Consts.MESSAGE_TYPE_OTHER;
import static com.spexco.msgframework.utils.Consts.MESSAGE_TYPE_PICTURE;
import static com.spexco.msgframework.utils.Consts.MESSAGE_TYPE_VIDEO;
import static com.spexco.msgframework.utils.UserInfo.database;
import static com.spexco.msgframework.utils.Utility.convertDateToString;
import static com.spexco.msgframework.utils.Utility.getContactInfo;
import static com.spexco.msgframework.utils.Utility.getTimeAgo;

public class MessageInfoActivity extends AppCompatActivity implements BaseParser.UpdateListener {

    private LinearLayout llSeen;
    private RecyclerView rvSeen;

    private LinearLayout llDelivered;
    private RecyclerView rvDelivered;

    private RecyclerView rvOneToOne;

    public static Message message;

    private TextView tvBody;
    private TextView tvTime;
    private RelativeLayout rlAllView;
    private ImageView imageView;
    private ImageView ivVideo;
    private ImageView ivMessageStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_info);

        llSeen = findViewById(R.id.ll_seen);
        rvSeen = findViewById(R.id.rv_seen);
        rvSeen.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        llDelivered = findViewById(R.id.ll_delivered);
        rvDelivered = findViewById(R.id.rv_delivered);
        rvDelivered.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        rvOneToOne = findViewById(R.id.rv_one_to_one);
        rvOneToOne.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        setMessageView();

        if (message.isGroup) {
            rvOneToOne.setVisibility(View.GONE);
        } else {
            llDelivered.setVisibility(View.GONE);
            llSeen.setVisibility(View.GONE);
            setMessageInfo();
        }
    }

    public void onBack(View view) {
        finish();
    }

    private void setMessageView() {

        imageView = findViewById(R.id.iv_image);
        ivVideo = findViewById(R.id.iv_image_video);
        tvBody = findViewById(R.id.tv_body);
        tvTime = findViewById(R.id.tv_time);
        ivMessageStatus = findViewById(R.id.iv_message_status);

        bind();
    }

    void bind() {

        if (message.MessageType == MESSAGE_TYPE_PICTURE || message.MessageType == MESSAGE_TYPE_VIDEO || message.MessageType == MESSAGE_TYPE_LOCATION) {
            if (message.MediaURLs != null && message.MediaURLs.size() > 0) {
                imageView.setVisibility(View.VISIBLE);
                Glide.with(getApplicationContext()).load(message.MediaURLs.get(0)).into(imageView);
            }

            ivVideo.setVisibility(message.MessageType == MESSAGE_TYPE_VIDEO ? View.VISIBLE : View.GONE);

        } else {
            imageView.setVisibility(View.GONE);
            ivVideo.setVisibility(View.GONE);
        }

        if (message.MessageType == MESSAGE_TYPE_OTHER) {
            Drawable img = ContextCompat.getDrawable(getApplicationContext(), FileUtils.getMimetypeImage(message.Body));
            img.setBounds(0, 0, leftImageDp, leftImageDp);
            tvBody.setCompoundDrawables(img, null, null, null);
        } else if (message.MessageType == MESSAGE_TYPE_CONTACT) {
            Drawable img = ContextCompat.getDrawable(getApplicationContext(), R.drawable.default_user);
            img.setBounds(0, 0, leftImageDp, leftImageDp);
            tvBody.setCompoundDrawables(img, null, null, null);

        } else {
            tvBody.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }

        if (message.MessageType == MESSAGE_TYPE_CONTACT) {
            tvBody.setVisibility(View.VISIBLE);
            tvBody.setText(getContactInfo(message.Body));
        } else if (message.MessageType != MESSAGE_TYPE_LOCATION && message.Body != null && message.Body.length() > 0) {
            tvBody.setVisibility(View.VISIBLE);
            tvBody.setText(message.Body);
        } else {
            tvBody.setVisibility(View.GONE);
            tvBody.setText("");
        }

        tvTime.setText(convertDateToString(message.CreateDate, "HH:mm"));

        if (message.isGroup != null && message.isGroup) {
            onUpdate(BaseParser.UpdateType.MESSAGE_STATUS);
        } else {
            ivMessageStatus.setImageResource(message.getMessageStatusResource());
        }
    }

    private void setMessageInfo() {

        database.messageModel().getLiveMessageById(message.MessageID).observe(this, new Observer<Message>() {
            @Override
            public void onChanged(@Nullable Message message) {

                if (message == null)
                    return;

                List<String> dateList = new ArrayList<>();
                dateList.add(message.Status == MESSAGE_STATUS_SEEN ? getTimeAgo(message.SeenDate) : "...");
                dateList.add(message.Status >= MESSAGE_STATUS_DELIVERED ? getTimeAgo(message.DeliverDate) : "...");
                MessageInfoAdapter adapter = new MessageInfoAdapter(null, dateList, false);
                rvOneToOne.setAdapter(adapter);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseParser.addListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BaseParser.removeListener(this);
    }

    @Override
    public String getUniqueID() {
        return message.MessageID;
    }

    @Override
    public void onUpdate(BaseParser.UpdateType type) {

        if (type == BaseParser.UpdateType.MESSAGE_STATUS) {

            MessageApplication.mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    List<MessageStatus> messageStatuses = database.messageStatusModel().getMessageStatus(message.MessageID);

                    if (messageStatuses == null) {
                        return;
                    }

                    List<MessageStatus> seen = new ArrayList<>();
                    List<MessageStatus> delivered = new ArrayList<>();

                    int unseenCount = 0;
                    int undeliveredCount = 0;

                    for (MessageStatus ms : messageStatuses) {
                        if (ms.Status == MESSAGE_STATUS_SEEN) {
                            seen.add(ms);
                        } else if (ms.Status == MESSAGE_STATUS_DELIVERED) {
                            delivered.add(ms);
                        }

                        if (ms.Status != MESSAGE_STATUS_SEEN) {
                            unseenCount++;
                        }
                        if (ms.Status != MESSAGE_STATUS_DELIVERED && ms.Status != MESSAGE_STATUS_SEEN) {
                            undeliveredCount++;
                        }
                    }

                    if (unseenCount == 0) {
                        ivMessageStatus.setImageResource(R.drawable.ic_message_status_seen);
                    } else if (undeliveredCount == 0) {
                        ivMessageStatus.setImageResource(R.drawable.ic_message_status_delivered);
                    } else if (message.Status >= MESSAGE_STATUS_SEND) {
                        ivMessageStatus.setImageResource(R.drawable.ic_message_status_sent);
                    } else {
                        ivMessageStatus.setImageResource(R.drawable.ic_message_status_waiting);
                    }

                    if (seen.size() > 0) {
                        llSeen.setVisibility(View.VISIBLE);
                        MessageInfoAdapter adapter = new MessageInfoAdapter(seen, null, true);
                        rvSeen.setAdapter(adapter);
                    } else {
                        llSeen.setVisibility(View.GONE);
                    }

                    if (delivered.size() > 0) {
                        llSeen.setVisibility(View.VISIBLE);
                        MessageInfoAdapter adapter = new MessageInfoAdapter(delivered, null, true);
                        rvDelivered.setAdapter(adapter);
                    } else {
                        llDelivered.setVisibility(View.GONE);
                    }
                }
            });
        }

    }


}

