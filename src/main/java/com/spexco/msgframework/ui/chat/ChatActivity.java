package com.spexco.msgframework.ui.chat;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.Observer;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.service.notification.StatusBarNotification;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.spexco.baselibrary.utils.AlertUtil;
import com.spexco.baselibrary.utils.FileUtils;
import com.spexco.msgframework.MessageApplication;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Conversation;
import com.spexco.msgframework.db.Group;
import com.spexco.msgframework.db.GroupContactWriting;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.db.MessageStatus;
import com.spexco.msgframework.db.Participant;
import com.spexco.msgframework.socket.BaseParser;
import com.spexco.msgframework.socket.ResultParser;
import com.spexco.msgframework.ui.chat.forward.ForwardActivity;
import com.spexco.msgframework.ui.chat.media.ChatMediaActivity;
import com.spexco.msgframework.utils.Utility;
import android.provider.ContactsContract.CommonDataKinds.*;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE;
import static com.spexco.msgframework.utils.Consts.*;
import static com.spexco.msgframework.utils.UserInfo.context;
import static com.spexco.msgframework.utils.UserInfo.database;
import static com.spexco.msgframework.utils.UserInfo.getConversationHistory;
import static com.spexco.msgframework.utils.UserInfo.gson;
import static com.spexco.msgframework.utils.UserInfo.myUserID;
import static com.spexco.msgframework.utils.UserInfo.sendToSocket;
import static com.spexco.msgframework.utils.Utility.isValid;
import static com.spexco.msgframework.utils.Utility.sendWritingInfo;
import static com.spexco.msgframework.utils.Utility.updateConversation;

public class ChatActivity extends Activity implements LifecycleOwner, ChatAdapter.MessageListener, BaseParser.UpdateListener {

    public static boolean isGroupChat = false;
    private static int myRole = -1;
    private List<Participant> participants = new ArrayList<>();

    public static String uniqueID;
    public static boolean isOpen = false;

    public Conversation conversation;
    public Group group;

    private boolean first = true;

    //topbar
    private ImageButton btnBack;
    private ImageView ivConversation;
    private TextView tvConversationName;
    private TextView tvConversationInfo;

    //center
    private RecyclerView recyclerView;
    private ChatAdapter adapter;

    //bottom
    private ImageView ivBottom;
    public EditText etMessage;
    private ImageButton btnSend;
    private ImageButton btnAdd;
    private static TextView tvRemoved;

    private NotificationManager notificationManager;
    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        //topbar
        btnBack = findViewById(R.id.btn_back);
        ivConversation = findViewById(R.id.iv_conversation_image);
        tvConversationName = findViewById(R.id.tv_conversation_name);
        tvConversationInfo = findViewById(R.id.tv_conversation_info);

        //center
        ivBottom = findViewById(R.id.ivBottom);
        recyclerView = findViewById(R.id.rv_chat);
        adapter = new ChatAdapter(this, this, new ArrayList<Message>());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == SCROLL_STATE_IDLE) {
                    if (recyclerView.canScrollVertically(1)) {
                        ivBottom.setVisibility(View.VISIBLE);
                    } else {
                        ivBottom.setVisibility(View.GONE);
                    }
                }
            }
        });

        //bottom
        etMessage = findViewById(R.id.et_message);
        btnSend = findViewById(R.id.btn_send);
        btnAdd = findViewById(R.id.btn_add);
        tvRemoved = findViewById(R.id.tv_removed);

        setBtnAdd();

        if (isGroupChat) {
            loadGroupChat();
        } else {
            loadConversation();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setRole();
        if (!first) {
            setMessages();
        }
        BaseParser.addListener(this);
        isOpen = true;
        clearNotifications();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BaseParser.removeListener(this);
        isOpen = false;
    }

    @Override
    public void onUpdate(BaseParser.UpdateType type) {
        switch (type) {
            case MESSAGE:
                setMessages();
                break;
            case MESSAGE_STATUS:
                break;
            case ROLE:
                setRole();
                break;
        }
    }

    @Override
    public String getUniqueID() {
        return uniqueID;
    }

    private void clearNotifications() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            for (StatusBarNotification notification : notificationManager.getActiveNotifications()) {
                if (notification.getTag().contains(uniqueID)) {
                    notificationManager.cancel(notification.getTag(), notification.getId());
                }
            }
        }
    }

    private void setMessages() {
        MessageApplication.mainHandler.post(setMessagesRunnable);
    }

    private Runnable setMessagesRunnable = new Runnable() {
        @Override
        public void run() {
            if (isGroupChat && group != null) {
                if (group.clearDate == null) {
                    adapter.setMessages(database.messageModel().getMessagesInGroupChat(group.GroupID));
                } else {
                    adapter.setMessages(database.messageModel().getMessagesInGroupChat(group.GroupID, group.clearDate));
                }
            } else if (conversation != null) {
                if (conversation.clearDate == null) {
                    adapter.setMessages(database.messageModel().getMessagesInConversation(conversation.ConversationID));
                } else {
                    adapter.setMessages(database.messageModel().getMessagesInConversation(conversation.ConversationID, conversation.clearDate));
                }
            }

            if (!recyclerView.canScrollVertically(1)) {
                recyclerView.smoothScrollToPosition(0);
            }
        }
    };

    public void smoothScrollToBottom(View view) {
        recyclerView.smoothScrollToPosition(0);
    }

    public void setRole() {
        if (isGroupChat) {
            MessageApplication.mainHandler.post(setRoleRunnable);
        }
    }

    private Runnable setRoleRunnable = new Runnable() {
        @Override
        public void run() {
            Participant participant = database.participantModel().getParticipant(uniqueID, myUserID);
            if (participant != null) {
                myRole = participant.Role;
            } else {
                myRole = PARTICIPANT_ROLE_DELETED;
            }
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (myRole == PARTICIPANT_ROLE_DELETED) {
                        tvRemoved.setText("Bu gruba mesaj gönderemezsiniz çünkü artık bir katılımcısı değilsiniz.");
                        tvRemoved.setVisibility(View.VISIBLE);
                    } else if (group.MessageAuth == 0 && myRole > PARTICIPANT_ROLE_ADMIN) {
                        tvRemoved.setText("Bu gruba sadece yöneticiler mesaj gönderebilir.");
                        tvRemoved.setVisibility(View.VISIBLE);
                    } else {
                        tvRemoved.setVisibility(View.GONE);
                    }
                }
            });
        }
    };

    private void loadConversation() {
        database.conversationModel().getConversationWithInfo(uniqueID).observe(this, new Observer<Conversation>() {
            @Override
            public void onChanged(@Nullable Conversation conversationWithInfo) {

                conversation = conversationWithInfo;

                if (conversation != null) {
                    setTopBar();

                    if (first) {
                        first = false;
                        sendMessageStatus();
                        setMessages();
                        setBottom();
                        database.conversationModel().getConversationWriting(conversation.ContactID).observe(ChatActivity.this, new Observer<Boolean>() {
                            @Override
                            public void onChanged(@Nullable Boolean isWriting) {
                                if (isWriting != null && isWriting) {
                                    tvConversationInfo.setText("yazıyor...");
                                } else {
                                    tvConversationInfo.setText("Kişi bilgisi için dokunun");
                                }
                            }
                        });
                    }
                }
            }
        });
    }

    private void loadGroupChat() {
        database.groupModel().getGroupWithInfo(uniqueID).observe(this, new Observer<Group>() {
            @Override
            public void onChanged(@Nullable Group groupWithInfo) {

                if (group != null && groupWithInfo != null && group.MessageAuth != groupWithInfo.MessageAuth) {
                    MessageApplication.mainHandler.post(setRoleRunnable);
                }

                group = groupWithInfo;

                if (group != null) {
                    setTopBar();

                    if (first) {
                        first = false;
                        sendMessageStatus();
                        setMessages();
                        setBottom();
                        database.conversationModel().getGroupConversationWriting(group.GroupID).observe(ChatActivity.this, new Observer<GroupContactWriting>() {
                            @Override
                            public void onChanged(@Nullable GroupContactWriting groupContactWriting) {
                                if (groupContactWriting != null && groupContactWriting.isWriting) {
                                    tvConversationInfo.setText(database.contactModel().getContactVisibleName(groupContactWriting.ContactID) + " yazıyor...");
                                } else  {
                                    tvConversationInfo.setText("Grup bilgisi için dokunun");
                                }
                            }
                        });
                        database.participantModel().getLiveParticipants(group.GroupID).observe(ChatActivity.this, new Observer<List<Participant>>() {
                            @Override
                            public void onChanged(@Nullable List<Participant> participants) {
                                ChatActivity.this.participants = participants;
                            }
                        });
                    }
                }
            }
        });
    }

    private void sendMessageStatus() {
        int unreadMessagesCount = isGroupChat ? database.messageModel().getUnreadMessagesCountInGroup(myUserID, uniqueID) :
                database.messageModel().getUnreadMessagesCountInConversation(myUserID, uniqueID);
        if (unreadMessagesCount > 0) {
            List<ResultParser.MessageStatusInfo> info = new ArrayList<>();
            ResultParser.MessageStatusInfo msi = new ResultParser.MessageStatusInfo(uniqueID, isGroupChat, MESSAGE_STATUS_SEEN, isGroupChat ? "" : conversation.ContactID);
            info.add(msi);
            updateConversation(info);

            if (isGroupChat) {
                database.messageModel().userSetMessageStatusSeen(uniqueID, "", MESSAGE_STATUS_SEEN, msi.UpdateDate);
            } else {
                database.messageModel().userSetMessageStatusSeen(myUserID, uniqueID, MESSAGE_STATUS_SEEN, msi.UpdateDate);
            }
        }
    }

    private void setTopBar() {

        String photoURL = isGroupChat ? group.PhotoURL : conversation.PhotoURL;
        String visibleName = isGroupChat ? group.GroupName : conversation.VisibleName;

        if ( isValid(photoURL) ) {
            Glide.with(context).load(photoURL).into(ivConversation);
        } else {
            Glide.with(context).load(isGroupChat ? R.drawable.ic_default_group : R.drawable.ic_default_user).into(ivConversation);
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tvConversationInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myRole == PARTICIPANT_ROLE_DELETED) {
                    return;
                }
                Intent intent = new Intent(ChatActivity.this, ChatInfoActivity.class);
                ChatInfoActivity.isGroup = isGroupChat;
                ChatInfoActivity.uniqueID = uniqueID;
                startActivity(intent);
            }
        });

        tvConversationName.setText(visibleName);
    }

    private void setBottom() {
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String text = etMessage.getText().toString().trim();
                //RSAUtils.testEncryptData(text);
                if (text.length() == 0) {
                    Toast.makeText(getApplicationContext(), "Lütfen mesajınızı giriniz.", Toast.LENGTH_SHORT).show();
                    return;
                }
                etMessage.setText("");
                Message message = new Message(isGroupChat ? group.GroupID : conversation.ContactID, MESSAGE_TYPE_TEXT, text, isGroupChat ? "" : conversation.ConversationID, isGroupChat);
                sendMessage(message);
            }
        });

        etMessage.addTextChangedListener(textWatcher);
    }

    public void sendMessage(Message message) {
        if (sendToSocket(gson.toJson(message, Message.class))) {
            message.Status = MESSAGE_STATUS_SEND;
            message.isSync = true;
        }
        stopWritingHandler.removeCallbacks(sendStopWriting);
        sendStopWriting.run();
        database.insertMessage(message);

        if (isGroupChat) {
            ArrayList<MessageStatus> messageStatusList = new ArrayList<>();
            for (Participant p : participants) {
                if (! p.UserID.contentEquals(myUserID)) {
                    MessageStatus m = new MessageStatus(message.MessageID, p.UserID, MESSAGE_STATUS_SEND, null, null, message.To);
                    m.initPK();
                    messageStatusList.add(m);
                }
            }
            database.messageStatusModel().insertMessageStatus(messageStatusList);
        }
        setMessages();
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { }

        @Override
        public void afterTextChanged(Editable s) {
            if (!lastSentWriting && s.toString().length() > 0) {
                lastSentWriting = true;
                sendWritingInfo(isGroupChat ? group.GroupID : conversation.ContactID, VALUE_TASK_START_TYPING, isGroupChat);
            }
            stopWritingHandler.removeCallbacks(sendStopWriting);
            stopWritingHandler.postDelayed(sendStopWriting, 2500);
        }
    };

    private boolean lastSentWriting = false;
    private Handler stopWritingHandler = new Handler();

    private Runnable sendStopWriting = new Runnable() {
        @Override
        public void run() {
            lastSentWriting = false;
            sendWritingInfo(isGroupChat ? group.GroupID : conversation.ContactID, VALUE_TASK_STOP_TYPING, isGroupChat);
        }
    };

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return lifecycleRegistry;
    }

    @Override
    public void onInfo(Message message) {
        Intent intent = new Intent(ChatActivity.this, MessageInfoActivity.class);
        MessageInfoActivity.message = message;
        startActivity(intent);
    }

    @Override
    public void onForward(Message message) {
        Intent intent = new Intent(ChatActivity.this, ForwardActivity.class);
        ForwardActivity.message = message;
        ForwardActivity.chatActivity = this;
        startActivity(intent);
    }

    @Override
    public void onLoadMore(Message message) {

        if ((conversation != null && conversation.clearDate == null) || (group != null && group.clearDate == null)) {
            String request = getConversationHistory(message.CreateDate, uniqueID);
            sendToSocket(request);
        }
    }

    /**
     * Message Type Menu
     */

    public static final int REQUEST_IMAGE_CAPTURE = 1000;
    private static final int REQUEST_VIDEO_CAPTURE = 1010;
    public static final int REQUEST_GALLERY = 1020;
    private static final int REQUEST_FILE = 1030;
    private static final int REQUEST_CONTACT = 1040;

    private void setBtnAdd() {
        final PopupMenu popup = new PopupMenu(ChatActivity.this, btnAdd);
        popup.inflate(R.menu.message_type);
        setForceShowIcon(popup);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        int i = item.getItemId();
                        if (i == R.id.message_type_photo) {
                            onPhoto();
                            return true;
                        } else if (i == R.id.message_type_video) {
                            onVideo();
                            return true;
                        } else if (i == R.id.message_type_gallery) {
                            onGallery();
                            return true;
                        } else if (i == R.id.message_type_file) {
                            onFile();
                            return true;
                        } else if (i == R.id.message_type_location) {
                            onLocation();
                            return true;
                        } else if (i == R.id.message_type_contact) {
                            onContact();
                            return true;
                        } else {
                            return false;
                        }
                    }
                });
                popup.show();
            }
        });
    }

    private void onContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, REQUEST_CONTACT);
    }

    private void onLocation() {
        Intent intent = new Intent(ChatActivity.this, ChatLocationActivity.class);
        ChatLocationActivity.chatActivity = this;
        startActivity(intent);
    }

    private void onFile() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            startActivityForResult(Intent.createChooser(intent, "Seçiniz"), REQUEST_FILE);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            startActivityForResult(intent, REQUEST_FILE);
        }
    }

    private void onGallery() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/* video/*");
            startActivityForResult(Intent.createChooser(intent, "Seçiniz"), REQUEST_GALLERY);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[] {"image/*", "video/*"});
            startActivityForResult(intent, REQUEST_GALLERY);
        }
    }

    private void onVideo() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            File file = new File(LOCAL_FILE_DIR + "video.mp4");
            Uri uri = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT,10);
            startActivityForResult(intent, REQUEST_VIDEO_CAPTURE);
        } else {
            Toast.makeText(getApplicationContext(), "Cihazınız video çekimini desteklemiyor.", Toast.LENGTH_SHORT).show();
        }
    }

    private void onPhoto() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                File file = new File(LOCAL_FILE_DIR + "image.jpg");
                Uri uri = FileProvider.getUriForFile(getApplicationContext(), getPackageName() + ".provider", file);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            } else {
                Toast.makeText(getApplicationContext(), "Cihazınız fotoğraf çekimini desteklemiyor.", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (resultCode == RESULT_OK) {

                ChatMediaActivity.chatActivity = this;
                ChatMediaActivity.text = etMessage.getText().toString();

                if (requestCode == REQUEST_IMAGE_CAPTURE) {

                    String targetDir = LOCAL_FILE_DIR + "/";
                    String fileName = UUID.randomUUID().toString();

                    File file = new File(LOCAL_FILE_DIR + "image.jpg");
                    FileUtils.writeJpgFile(file, targetDir, fileName);
                    ChatMediaActivity.type = 1;
                    ArrayList<String> filePaths = new ArrayList<>();
                    filePaths.add(targetDir + fileName + ".jpg");
                    ChatMediaActivity.filePaths = filePaths;
                    ChatMediaActivity.chatActivity = this;
                    ChatMediaActivity.text = etMessage.getText().toString();
                    ChatMediaActivity.isImageFromCamera = true;
                    Intent intent = new Intent(ChatActivity.this, ChatMediaActivity.class);
                    startActivity(intent);

                } else if (requestCode == REQUEST_VIDEO_CAPTURE) {

                    ChatMediaActivity.type = 2;
                    ArrayList<String> filePaths = new ArrayList<>();
                    filePaths.add(LOCAL_FILE_DIR + "video.mp4");
                    ChatMediaActivity.filePaths = filePaths;
                    ChatMediaActivity.chatActivity = this;
                    ChatMediaActivity.text = etMessage.getText().toString();
                    Intent intent = new Intent(ChatActivity.this, ChatMediaActivity.class);
                    startActivity(intent);

                } else if (requestCode == REQUEST_GALLERY) {

                    if (data != null) {
                        if (data.getData() != null){
                            Uri uri = data.getData();

                            String filePath = FileUtils.getPath(getApplicationContext(), uri);
                            File file = new File(filePath);
                            String mimetype = FileUtils.getMimeType(file);
                            int messageType = getMessageType(mimetype);

                            if (messageType < 0) {
                                return;
                            }

                            ChatMediaActivity.type = messageType == MESSAGE_TYPE_PICTURE ? 1 : 2;
                            ArrayList<String> filePaths = new ArrayList<>();
                            filePaths.add(filePath);
                            ChatMediaActivity.filePaths = filePaths;
                            ChatMediaActivity.chatActivity = this;
                            ChatMediaActivity.text = etMessage.getText().toString();
                            ChatMediaActivity.isImageFromCamera = false;
                            Intent intent = new Intent(ChatActivity.this, ChatMediaActivity.class);
                            startActivity(intent);
                        }
                    }

                } else if (requestCode == REQUEST_FILE) {

                    if (data != null) {
                        if (data.getData() != null){
                            Uri uri = data.getData();

                            String filePath = FileUtils.getPath(getApplicationContext(), uri);
                            final File file = new File(filePath);
                            String mimetype = FileUtils.getMimeType(file);
                            final int messageType = getMessageType(mimetype);
                            if (messageType < 0) {

                                AlertUtil.showAlert(ChatActivity.this, "Dosya Gönder", "Bu dosyayı göndermek istediğinizden emin misiniz?",
                                        "Evet", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                uploadFile(file);
                                            }
                                        },
                                        "İptal", null);

                                return;
                            } else {
                                ChatMediaActivity.type = messageType == MESSAGE_TYPE_PICTURE ? 1 : 2;
                                ArrayList<String> filePaths = new ArrayList<>();
                                filePaths.add(filePath);
                                ChatMediaActivity.filePaths = filePaths;
                                ChatMediaActivity.chatActivity = this;
                                ChatMediaActivity.text = etMessage.getText().toString();
                                ChatMediaActivity.isImageFromCamera = false;
                                Intent intent = new Intent(ChatActivity.this, ChatMediaActivity.class);
                                startActivity(intent);
                            }

                        }
                    }

                } else if (requestCode == REQUEST_CONTACT) {

                    ContentResolver cr = getContentResolver();
                    Uri contactUri = data.getData();
                    Cursor cursor = getContentResolver().query(contactUri, null,null, null, null);

                    if (cursor != null && cursor.moveToFirst()) {
                        String contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                        String displayName = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));

                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty(KEY_DISPLAY_NAME, displayName);

                        JsonArray phonesHomeArr = new JsonArray();
                        JsonArray phonesMobileArr = new JsonArray();
                        JsonArray phonesWorkArr = new JsonArray();
                        JsonArray phonesOtherArr = new JsonArray();
                        Cursor phones = cr.query(Phone.CONTENT_URI, null, Phone.CONTACT_ID + " = " + contactId, null, null);
                        while (phones.moveToNext()) {
                            String number = phones.getString(phones.getColumnIndex(Phone.NUMBER));
                            int type = phones.getInt(phones.getColumnIndex(Phone.TYPE));
                            switch (type) {
                                case Phone.TYPE_HOME:
                                    phonesHomeArr.add(number);
                                    break;
                                case Phone.TYPE_MOBILE:
                                    phonesMobileArr.add(number);
                                    break;
                                case Phone.TYPE_WORK:
                                    phonesWorkArr.add(number);
                                    break;
                                default:
                                    phonesOtherArr.add(number);
                                    break;
                            }
                        }
                        phones.close();
                        jsonObject.add(KEY_PHONES_HOME, phonesHomeArr);
                        jsonObject.add(KEY_PHONES_MOBILE, phonesMobileArr);
                        jsonObject.add(KEY_PHONES_WORK, phonesWorkArr);
                        jsonObject.add(KEY_PHONES_OTHER, phonesOtherArr);

                        Cursor emails = cr.query(Email.CONTENT_URI, null, Email.CONTACT_ID + " = " + contactId, null, null);
                        JsonArray emailsHomeArr = new JsonArray();
                        JsonArray emailsMobileArr = new JsonArray();
                        JsonArray emailsWorkArr = new JsonArray();
                        JsonArray emailsOtherArr = new JsonArray();
                        while (emails.moveToNext()) {
                            String email = emails.getString(emails.getColumnIndex(Email.ADDRESS));
                            int type = emails.getInt(emails.getColumnIndex(Email.TYPE));
                            switch (type) {
                                case Email.TYPE_HOME:
                                    emailsHomeArr.add(email);
                                    break;
                                case Email.TYPE_MOBILE:
                                    emailsMobileArr.add(email);
                                    break;
                                case Email.TYPE_WORK:
                                    emailsWorkArr.add(email);
                                    break;
                                default:
                                    emailsOtherArr.add(email);
                                    break;
                            }
                        }
                        emails.close();
                        jsonObject.add(KEY_EMAILS_HOME, emailsHomeArr);
                        jsonObject.add(KEY_EMAILS_MOBILE, emailsMobileArr);
                        jsonObject.add(KEY_EMAILS_WORK, emailsWorkArr);
                        jsonObject.add(KEY_EMAILS_OTHER, emailsOtherArr);

                        Message message = new Message(isGroupChat ? group.GroupID : conversation.ContactID, MESSAGE_TYPE_CONTACT, jsonObject.toString(), isGroupChat ? "" : conversation.ConversationID, isGroupChat);
                        sendMessage(message);
                    }

                    cursor.close();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void uploadFile(final File file) {

        final ProgressDialog progress = ProgressDialog.show(ChatActivity.this, "", "Gönderiliyor...");
        Utility.uploadFile(new Utility.UploadFileListener() {
            @Override
            public void onResponse(boolean success, String description) {
                if (success) {
                    Message message = new Message(isGroupChat ? group.GroupID : conversation.ContactID, MESSAGE_TYPE_OTHER, file.getName(), isGroupChat ? "" : conversation.ConversationID, isGroupChat);
                    message.MediaURLs = new ArrayList<>();
                    message.MediaURLs.add(description);
                    sendMessage(message);
                    progress.dismiss();
                } else {
                    progress.dismiss();
                    Toast.makeText(getApplicationContext(), "Mesaj gönderilemedi", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }, file);
    }

    public static int getMessageType(String mimetype) {

        if (mimetype.contains("image")) {
            return MESSAGE_TYPE_PICTURE;
        } else if (mimetype.contains("video")) {
            return MESSAGE_TYPE_VIDEO;
        }

        return -1;
    }

    private void setForceShowIcon(PopupMenu popupMenu) {
        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
