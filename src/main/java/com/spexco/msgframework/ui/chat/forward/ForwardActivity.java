package com.spexco.msgframework.ui.chat.forward;

import android.app.ProgressDialog;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.spexco.baselibrary.activity.BaseActivity;
import com.spexco.baselibrary.utils.FileUtils;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Group;
import com.spexco.msgframework.ui.chat.ChatActivity;
import com.spexco.msgframework.db.BaseConversation;
import com.spexco.msgframework.db.Conversation;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.db.MessageStatus;
import com.spexco.msgframework.db.Participant;
import com.spexco.msgframework.socket.SpexMesajSocketClient;
import com.spexco.msgframework.ui.main.adapters.ConversationAdapter;
import com.spexco.msgframework.utils.Utility;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.spexco.msgframework.utils.Consts.MESSAGE_STATUS_SEND;
import static com.spexco.msgframework.utils.Consts.MESSAGE_TYPE_OTHER;
import static com.spexco.msgframework.utils.Consts.MESSAGE_TYPE_TEXT;
import static com.spexco.msgframework.utils.UserInfo.database;
import static com.spexco.msgframework.utils.UserInfo.getUserID;
import static com.spexco.msgframework.utils.UserInfo.gson;
import static com.spexco.msgframework.utils.UserInfo.myUserID;
import static com.spexco.msgframework.utils.UserInfo.sendToSocket;
import static com.spexco.msgframework.utils.Utility.isValid;

public class ForwardActivity extends BaseActivity implements ConversationAdapter.ConversationListListener, LifecycleOwner {

    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

    private ConversationAdapter adapter;
    private RecyclerView recyclerView;
    private EditText etSearch;
    private ProgressDialog progress;

    public static ChatActivity chatActivity;
    public static Message message;

    private List<Group> groups = new ArrayList<>();
    private List<Conversation> conversations = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forward);

        recyclerView = findViewById(R.id.rv_conversation);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new ConversationAdapter(this, this);
        recyclerView.setAdapter(adapter);
        etSearch = findViewById(R.id.et_search);
        etSearch.addTextChangedListener(textWatcher);

        database.groupModel().getAllGroups().observe(this, new Observer<List<Group>>() {
            @Override
            public void onChanged(@Nullable List<Group> gs) {
                groups = gs;
                updateConversations();
            }
        });

        database.conversationModel().getAllConversations().observe(this, new Observer<List<Conversation>>() {
            @Override
            public void onChanged(@Nullable List<Conversation> cs) {
                conversations = cs;
                updateConversations();
            }
        });

        if (!SpexMesajSocketClient.getInstance().isConnected()) {
            String userID = getUserID();
            if (isValid(userID)) {
                SpexMesajSocketClient.getInstance().connect(myUserID);
            }
        }
    }

    public void updateConversations() {

        List<BaseConversation> base = new ArrayList<>();

        for (Conversation conversation : conversations) {
            conversation.lastMessage = database.messageModel().getLastMessageInConversation(conversation.ConversationID);
        }
        for (Group group : groups) {
            group.lastMessage = database.messageModel().getLastMessageInGroup(group.GroupID);
        }
        base.addAll(conversations);
        base.addAll(groups);
        Collections.sort(base);

        adapter.setConversations(base);
        adapter.filter(etSearch.getText().toString());
    }

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            adapter.filter(s.toString());
        }
    };

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    public void onCancel(View view) {
        finish();
    }

    @Override
    public void onSelect(BaseConversation conversation) {

        final Intent intent = getIntent();
        if (Intent.ACTION_SEND.equals(intent.getAction()) && intent.getExtras() != null) {
            Bundle extras = intent.getExtras();
            if (extras.containsKey(Intent.EXTRA_STREAM)) {
                Uri uri = extras.getParcelable(Intent.EXTRA_STREAM);
                String scheme = uri.getScheme();
                if ((scheme.equals("content") || scheme.equals("file"))) {
                    File file = null;
                    try {
                        file = FileUtils.getFile(getApplicationContext(), uri);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (file != null && !isGoogleDriveDocument(uri)) {
                        if (progress == null) {
                            progress = ProgressDialog.show(ForwardActivity.this, "", "Gönderiliyor...");
                        } else {
                            progress.show();
                        }
                        uploadFile(file, conversation);
                    }
                }
            } else if (extras.containsKey(Intent.EXTRA_TEXT)) {
                String body = extras.getString(Intent.EXTRA_TEXT);
                Message msg = null;
                if (conversation instanceof Group) {
                    msg = new Message(((Group) conversation).GroupID, MESSAGE_TYPE_TEXT, body, "", true);
                } else if (conversation instanceof Conversation) {
                    Conversation c = (Conversation) conversation;
                    msg = new Message(c.ContactID, MESSAGE_TYPE_TEXT, body, c.ConversationID, false);
                }
                sendMessage(msg);
            }
        } else if (message != null) {
            Message msg = new Message(message);
            msg.From = myUserID;
            if (conversation instanceof Group) {
                msg.To = ((Group) conversation).GroupID;
                msg.ConversationID = "";
                msg.isGroup = true;
            } else if (conversation instanceof Conversation) {
                Conversation c = (Conversation) conversation;
                msg.To = c.ContactID;
                msg.ConversationID = c.ConversationID;
                msg.isGroup = false;
            }

            sendMessage(msg);
        }
    }

    private void uploadFile(final File file, final BaseConversation conversation) {
        Utility.uploadFile(new Utility.UploadFileListener() {
            @Override
            public void onResponse(boolean success, String description) {
                if (success) {

                    String mimetype = FileUtils.getMimeType(file);
                    String body = "";

                    int messageType = ChatActivity.getMessageType(mimetype);
                    if (messageType < 0) {
                        messageType = MESSAGE_TYPE_OTHER;
                        body = file.getName();
                    }

                    Message msg = null;
                    if (conversation instanceof Group) {
                        msg = new Message(((Group) conversation).GroupID, messageType, body, "", true);
                    } else if (conversation instanceof Conversation) {
                        Conversation c = (Conversation) conversation;
                        msg = new Message(c.ContactID, messageType, body, c.ConversationID, false);
                    }
                    msg.MediaURLs = new ArrayList<>();
                    msg.MediaURLs.add(description);
                    sendMessage(msg);

                } else {
                    progress.dismiss();
                    Toast.makeText(getApplicationContext(), "Mesaj gönderilemedi", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }, file);
    }

    public static boolean isGoogleDriveDocument(Uri uri) {
        return uri.getAuthority().contains("com.google.android.apps.docs");
    }

    public void sendMessage(Message message) {
        if (sendToSocket(gson.toJson(message, Message.class))) {
            message.Status = MESSAGE_STATUS_SEND;
            message.isSync = true;
        }

        database.insertMessage(message);

        if (message.isGroup) {
            List<Participant> participants = database.participantModel().getParticipants(message.To);
            ArrayList<MessageStatus> messageStatusList = new ArrayList<>();
            for (Participant p : participants) {
                if (!p.UserID.contentEquals(myUserID)) {
                    MessageStatus m = new MessageStatus(message.MessageID, p.UserID, MESSAGE_STATUS_SEND, null, null, message.To);
                    m.initPK();
                    messageStatusList.add(m);
                }
            }
            database.messageStatusModel().insertMessageStatus(messageStatusList);
        }

        if (message.isGroup) {
            ChatActivity.isGroupChat = true;
            ChatActivity.uniqueID = message.To;
        } else {
            ChatActivity.isGroupChat = false;
            ChatActivity.uniqueID = message.ConversationID;
        }

        ForwardActivity.message = null;

        Intent intent = new Intent(this, ChatActivity.class);
        startActivity(intent);

        if (chatActivity != null) {
            chatActivity.finish();
            chatActivity = null;
        }
        finish();
    }

    @Override
    public void onLongSelect(BaseConversation conversation, View view) {

    }
}
