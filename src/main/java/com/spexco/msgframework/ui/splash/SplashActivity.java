package com.spexco.msgframework.ui.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.spexco.baselibrary.utils.FileUtils;
import com.spexco.baselibrary.utils.permission.PermissionRequester;
import com.spexco.baselibrary.utils.permission.RuntimePermissionResultListener;
import com.spexco.msgframework.MessageApplication;
import com.spexco.msgframework.R;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.socket.SpexMesajSocketClient;
import com.spexco.msgframework.ui.login.LoginActivity;
import com.spexco.msgframework.ui.main.MainActivity;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.spexco.msgframework.utils.Consts.LOCAL_FILE_DIR;
import static com.spexco.msgframework.utils.UserInfo.gson;
import static com.spexco.msgframework.utils.UserInfo.myUserID;
import static com.spexco.msgframework.utils.UserInfo.registerPushToken;
import static com.spexco.msgframework.utils.Utility.isValid;

public class SplashActivity extends Activity implements RuntimePermissionResultListener, SpexMesajSocketClient.SpexMesajSocketConnectionListener{

    private static final int SPLASH_DURATION = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            String msg = extras.getString("message");
            Log.e("testtest", "splash: " + msg);

            if (isValid(msg)) {
                Message message = gson.fromJson(msg, Message.class);
                boolean isGroup = message.isGroup;
                MessageApplication.waitingIsGroup = isGroup;
                MessageApplication.waitingForUniqueID = isGroup ? message.To : message.ConversationID;
            }
        }

        if (isValid(myUserID)) {
            SpexMesajSocketClient.getInstance().socketConnectionListener = this;
            SpexMesajSocketClient.getInstance().connect(myUserID);

        } else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    PermissionRequester.requestPermission(getApplicationContext(), new String[]{READ_CONTACTS, ACCESS_FINE_LOCATION, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE, RECORD_AUDIO}, SplashActivity.this);
                }
            }, SPLASH_DURATION);
        }
        
    }

    @Override
    public void onSocketConnectionOpen() {
        SpexMesajSocketClient.getInstance().socketConnectionListener = null;
        registerPushToken();
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSocketConnectionClose() {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onPermissionRejected() {
        finish();
    }

    @Override
    public void onPermissionGranted() {

        FileUtils.createFolderIfNotExist(LOCAL_FILE_DIR);

        onSocketConnectionClose();
    }
}
