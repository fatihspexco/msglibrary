package com.spexco.msgframework.ui.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.annotations.Expose;
import com.spexco.baselibrary.utils.SecurityUtils;
import com.spexco.baselibrary.utils.SharedPrefUtil;
import com.spexco.msgframework.BuildConfig;
import com.spexco.msgframework.R;
import com.spexco.msgframework.socket.SpexMesajSocketClient;
import com.spexco.msgframework.ui.main.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import static com.spexco.msgframework.MessageApplication.getVersionName;
import static com.spexco.msgframework.utils.Consts.KEY_LOGIN_TOKEN;
import static com.spexco.msgframework.utils.Consts.SERVICE_ADDRESS;
import static com.spexco.msgframework.utils.UserInfo.gson;
import static com.spexco.msgframework.utils.UserInfo.myUserID;
import static com.spexco.msgframework.utils.UserInfo.registerPushToken;
import static com.spexco.msgframework.utils.Utility.isValid;

public class LoginActivity extends AppCompatActivity implements SpexMesajSocketClient.SpexMesajSocketConnectionListener {

    private EditText etUsername;
    private EditText etPassword;
    private TextView tvVersion;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SpexMesajSocketClient.getInstance().socketConnectionListener = this;

        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        tvVersion = findViewById(R.id.tv_version);

        tvVersion.setText("V" + getVersionName());

        findViewById(R.id.btn_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                if (isValid(username) && isValid(password)) {
                    progress = ProgressDialog.show(LoginActivity.this, "", "Bağlanıyor...");
                    password = SecurityUtils.encryptAES(username, new StringBuilder(username).reverse().toString(), password);
                    login(username, password);
                } else {
                    Toast.makeText(getApplicationContext(), "Lütfen kullanıcı adı ve şifre giriniz.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (isValid(myUserID)) {
            etUsername.setText(myUserID);
        }
    }

    private void login(final String username, String password) {
        String url = SERVICE_ADDRESS + "Login";

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("UserID", username);
            jsonObject.put("Password", password);
            jsonObject.put("isMobile", "True");
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        AndroidNetworking.post(url)
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("testtest", response);
                        try {
                            LoginResult result = gson.fromJson(response, LoginResult.class);
                            if (result.Success) {
                                SharedPrefUtil.getInstance(getApplicationContext()).putString(KEY_LOGIN_TOKEN, result.Token);
                                SpexMesajSocketClient.getInstance().connect(username);
                            } else {
                                onSocketConnectionClose();
                                Toast.makeText(getApplicationContext(), result.Error, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            onSocketConnectionClose();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(getApplicationContext(), "Beklenmeyen bir hata oluştu, lütfen daha sonra tekrar deneyiniz.", Toast.LENGTH_LONG).show();
                        onSocketConnectionClose();
                    }
                });
    }

    private class LoginResult {
        @Expose
        public boolean Success;

        @Expose
        public String Error;

        @Expose
        public String Token;
    }

    @Override
    public void onSocketConnectionOpen() {
        SpexMesajSocketClient.getInstance().socketConnectionListener = null;
        progress.dismiss();
        registerPushToken();
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSocketConnectionClose() {
        progress.dismiss();
    }
}
