package com.spexco.msgframework.ui.main;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.utils.Utility;
import com.spexco.msgframework.R;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment {

    private EditText etVisibleName;
    private EditText etStatusText;
    private ImageView ivProfile;
    private Uri fileUri;
    private ProgressDialog progress;

    @Override
    public void onResume() {
        super.onResume();
        if (UserInfo.myContact != null) {

            if (Utility.isValid(UserInfo.myContact.VisibleName)) {
                etVisibleName.setText(UserInfo.myContact.VisibleName);
            }

            if (Utility.isValid(UserInfo.myContact.StatusText)) {
                etStatusText.setText(UserInfo.myContact.StatusText);
            }

            if (Utility.isValid(UserInfo.myContact.PhotoURL)) {
                Glide.with(UserInfo.context).load(UserInfo.myContact.PhotoURL).into(ivProfile);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        fileUri = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        etVisibleName = view.findViewById(R.id.et_visible_name);
        etStatusText = view.findViewById(R.id.et_status_text);
        ivProfile = view.findViewById(R.id.iv_profile_image);

        view.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (fileUri != null) {
                    progress = ProgressDialog.show(getActivity(), "", "Kaydediliyor...");
                    Utility.uploadFile(new Utility.UploadFileListener() {
                        @Override
                        public void onResponse(boolean success, String description) {
                            if (success) {
                                UserInfo.myContact.PhotoURL = description;
                                UserInfo.myContact.VisibleName = etVisibleName.getText().toString();
                                UserInfo.myContact.StatusText = etStatusText.getText().toString();
                                UserInfo.saveUser();
                            }
                            fileUri = null;
                            progress.dismiss();
                        }
                    }, new File(fileUri.getPath()));
                } else {
                    UserInfo.myContact.VisibleName = etVisibleName.getText().toString();
                    UserInfo.myContact.StatusText = etStatusText.getText().toString();
                    UserInfo.saveUser();
                }

            }
        });

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CropImage.activity().setCropShape(CropImageView.CropShape.OVAL)
                        .setAspectRatio(1, 1)
                        .start(getContext(), ProfileFragment.this);
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                fileUri = result.getUri();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Glide.with(UserInfo.context).load(fileUri).into(ivProfile);
                    }
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

}
