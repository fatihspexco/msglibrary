package com.spexco.msgframework.ui.main.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spexco.msgframework.db.Contact;
import com.spexco.msgframework.utils.Utility;
import com.spexco.msgframework.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by fatihinan on 04.12.2018.
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {

    private List<Contact> contacts = new ArrayList<>();
    private List<Contact> filteredContacts = new ArrayList<>();
    private final ContactListListener mListener;

    public ContactAdapter(ContactListListener listener) {
        mListener = listener;
    }

    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
        filteredContacts.addAll(contacts);
    }

    public void filter(String name) {
        filteredContacts.clear();

        if (name.length() > 0) {
            for (Contact c : contacts) {
                if (c.VisibleName.toLowerCase().contains(name.toLowerCase())) {
                    filteredContacts.add(c);
                }
            }
        } else {
            filteredContacts.addAll(contacts);
        }

        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = filteredContacts.get(position);
        holder.mVisibleNameView.setText(holder.mItem.VisibleName);
        holder.mStatusTextView.setText(holder.mItem.StatusText);

        if ( Utility.isValid(filteredContacts.get(position).PhotoURL) ) {
            Glide
                .with(holder.mProfileImageView)
                .load(filteredContacts.get(position).PhotoURL)
                .into(holder.mProfileImageView);
        } else {
            Glide
                    .with(holder.mProfileImageView)
                    .load(R.drawable.ic_default_user)
                    .into(holder.mProfileImageView);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onSelect(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredContacts != null ? filteredContacts.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mVisibleNameView;
        public final TextView mStatusTextView;
        public final CircleImageView mProfileImageView;
        public Contact mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mVisibleNameView = view.findViewById(R.id.tv_visible_name);
            mStatusTextView = view.findViewById(R.id.tv_status_text);
            mProfileImageView = view.findViewById(R.id.iv_profile_image);
        }
    }

    public interface ContactListListener {
        void onSelect(Contact contact);
    }
}
