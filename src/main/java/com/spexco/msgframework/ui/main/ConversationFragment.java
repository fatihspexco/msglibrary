package com.spexco.msgframework.ui.main;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.spexco.msgframework.db.BaseConversation;
import com.spexco.msgframework.db.Conversation;
import com.spexco.msgframework.db.Group;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.ui.chat.ChatActivity;
import com.spexco.msgframework.ui.main.adapters.ConversationAdapter;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConversationFragment extends Fragment implements ConversationAdapter.ConversationListListener {

    private ConversationAdapter adapter;
    private RecyclerView recyclerView;
    private EditText etSearch;

    public static boolean isOpen = false;
    public static ConversationFragment instance;

    private List<Group> groups = new ArrayList<>();
    private List<Conversation> conversations = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conversation, container, false);

        recyclerView = view.findViewById(R.id.rv_conversation);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ConversationAdapter(this, this);
        recyclerView.setAdapter(adapter);
        etSearch = view.findViewById(R.id.et_search);
        etSearch.addTextChangedListener(textWatcher);

        UserInfo.database.groupModel().getAllGroups().observe(this, new Observer<List<Group>>() {
            @Override
            public void onChanged(@Nullable List<Group> gs) {
                groups = gs;
                updateConversations();
            }
        });

        UserInfo.database.conversationModel().getAllConversations().observe(this, new Observer<List<Conversation>>() {
            @Override
            public void onChanged(@Nullable List<Conversation> cs) {
                conversations = cs;
                updateConversations();
            }
        });

        view.findViewById(R.id.tv_new_group).setOnClickListener(newGroup);

        instance = this;

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        isOpen = true;
        updateConversations();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onPause() {
        super.onPause();
        isOpen = false;
    }

    public void updateConversations() {

        List<BaseConversation> base = new ArrayList<>();

        for (Conversation conversation : conversations) {
            conversation.lastMessage = UserInfo.database.messageModel().getLastMessageInConversation(conversation.ConversationID);
        }
        for (Group group : groups) {
            group.lastMessage = UserInfo.database.messageModel().getLastMessageInGroup(group.GroupID);
        }
        base.addAll(conversations);
        base.addAll(groups);
        Collections.sort(base);

        adapter.setConversations(base);
        adapter.filter(etSearch.getText().toString());
    }

    public void updateLastMessage(Message message) {
        try {
            List<BaseConversation> conversations = adapter.getConversations();
            for (int i=0; i<conversations.size(); i++) {
                if ( (message.isGroup && conversations.get(i) instanceof Group && ((Group) conversations.get(i)).GroupID.contentEquals(message.To) ) ||
                        !message.isGroup && conversations.get(i) instanceof Conversation && ((Conversation) conversations.get(i)).ConversationID.contentEquals(message.ConversationID)) {
                    ((ConversationAdapter.ViewHolder)recyclerView.findViewHolderForAdapterPosition(i)).updateLastMessage(message);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { }

        @Override
        public void afterTextChanged(Editable s) {
            adapter.filter(s.toString());
        }
    };

    private View.OnClickListener newGroup = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), ContactActivity.class);
            startActivity(intent);
        }
    };

    @Override
    public void onSelect(BaseConversation conversation) {

        if (conversation instanceof Group) {
            ChatActivity.isGroupChat = true;
            ChatActivity.uniqueID = ((Group)conversation).GroupID;
        } else if (conversation instanceof Conversation) {
            ChatActivity.isGroupChat = false;
            ChatActivity.uniqueID = ((Conversation)conversation).ConversationID;
        }

        Intent intent = new Intent(getActivity(), ChatActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLongSelect(final BaseConversation conversation, View view) {


        //TODO: delete thşs toast
        if (conversation.isGroup()) {
            Toast.makeText(getContext(), ((Group) conversation).isLDAPGroup + "", Toast.LENGTH_SHORT).show();
        }

        PopupMenu popup = new PopupMenu(getContext(), view);
        popup.inflate(R.menu.conversation);

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int i = item.getItemId();
                if (i == R.id.conversation_clear) {
                    conversation.clearMessages();
                    return true;
                } else if (i == R.id.conversation_delete_conversation) {
                    conversation.deleteConversation();
                    return true;
                } else {
                    return false;
                }
            }
        });
        popup.show();
    }

}
