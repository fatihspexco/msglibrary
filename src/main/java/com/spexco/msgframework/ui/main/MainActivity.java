package com.spexco.msgframework.ui.main;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.spexco.baselibrary.activity.BaseActivityInterface;
import com.spexco.baselibrary.ui.ProgressDialogInterface;
import com.spexco.baselibrary.utils.DownloadFileFromURL;
import com.spexco.msgframework.MessageApplication;
import com.spexco.msgframework.ui.chat.ChatActivity;
import com.spexco.msgframework.R;
import com.spexco.msgframework.socket.ResultParser;

import static com.spexco.msgframework.MessageApplication.getVersionName;


public class MainActivity extends AppCompatActivity implements BaseActivityInterface {

    private TextView mTextMessage;
//    private ContactFragment contactFragment;
    private ConversationFragment conversationFragment;
    private ProfileFragment profileFragment;

    public static boolean isOpen = false;
    public static ResultParser.Version version;
    public static MainActivity instance;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            Fragment selectedFragment = null;

            int i = item.getItemId();//                case R.id.navigation_contacts:
//                    selectedFragment = contactFragment;
//                    break;
            if (i == R.id.navigation_conversations) {
                selectedFragment = conversationFragment;

            } else if (i == R.id.navigation_profile) {
                selectedFragment = profileFragment;

            }

            if (selectedFragment == null) {
                return false;
            }

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, selectedFragment);
            transaction.commit();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        instance = this;
        profileFragment = new ProfileFragment();
        conversationFragment = new ConversationFragment();
//        contactFragment = new ContactFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, conversationFragment);
        transaction.commit();

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_conversations);
        if (version != null) {
            versionControl(version);
            version = null;
        }

        if (MessageApplication.waitingForUniqueID.length() > 0) {

            ChatActivity.isGroupChat = MessageApplication.waitingIsGroup;
            ChatActivity.uniqueID = MessageApplication.waitingForUniqueID;

            Intent intent = new Intent(this, ChatActivity.class);
            startActivity(intent);

            MessageApplication.waitingForUniqueID = "";
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isOpen = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isOpen = false;
    }

    public void versionControl(ResultParser.Version version) {

        try {
            double serverVersion = version.Version;
            double currentVersion = Double.valueOf(getVersionName());

            if (serverVersion > currentVersion) {
                String url = version.Link;
                String desc = version.Description;
                showUpdatePopup(url, desc, version.isForced);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showUpdatePopup(final String url, final String desc, final boolean isForced) {

        MessageApplication.mainHandler.post(new Runnable() {
            @Override
            public void run() {
                if (isForced) {

                    AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Yeni Sürüm Mevcut")
                            .setMessage(desc).setPositiveButton("İNDİR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    new DownloadFileFromURL(MainActivity.this).execute(url);
                                }
                            }).setCancelable(false).create();
                    dialog.show();

                } else {

                    AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Yeni Sürüm Mevcut")
                            .setMessage(desc).setPositiveButton("İNDİR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                    startActivity(browserIntent);
                                }
                            }).setNegativeButton("Devam Et", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            }).setCancelable(false).create();
                    dialog.show();
                }
            }
        });
    }

    ProgressDialog mProgressDialog;
    @Override
    public void showLoading() {
        mProgressDialog = new ProgressDialogInterface.Builder().context(this).cancelable(false).message("Lütfen bekleyiniz.").create();
        mProgressDialog.show();
    }

    @Override
    public void closeLoading() {
        if(!isFinishing()){
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void startActivityWith(Intent intent) {
        startActivity(intent);
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }
}
