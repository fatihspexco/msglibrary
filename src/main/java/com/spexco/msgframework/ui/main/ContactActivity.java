package com.spexco.msgframework.ui.main;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.spexco.msgframework.db.Contact;
import com.spexco.msgframework.db.Conversation;
import com.spexco.msgframework.ui.chat.ChatActivity;
import com.spexco.msgframework.ui.main.adapters.ContactAdapter;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.R;
import com.spexco.msgframework.ui.creategroup.participant.CreateGroupActivity;

import java.util.List;


public class ContactActivity extends AppCompatActivity implements ContactAdapter.ContactListListener {

    private ContactAdapter adapter;
    private RecyclerView recyclerView;
    private EditText etSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_contact);

        recyclerView = findViewById(R.id.rv_contact);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter = new ContactAdapter(this);
        recyclerView.setAdapter(adapter);
        etSearch = findViewById(R.id.et_search);
        etSearch.addTextChangedListener(textWatcher);

        UserInfo.database.contactModel().getAllContactsLive(UserInfo.myUserID).observe(this, new Observer<List<Contact>>() {
            @Override
            public void onChanged(@Nullable List<Contact> contacts) {
                adapter.setContacts(contacts);
                adapter.filter(etSearch.getText().toString());
            }
        });
    }

    public void onNewGroup(View view) {
        Intent intent = new Intent(ContactActivity.this, CreateGroupActivity.class);
        CreateGroupActivity.previousActivity = this;
        startActivity(intent);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) { }

        @Override
        public void afterTextChanged(Editable s) {
            adapter.filter(s.toString());
        }
    };

    @Override
    public void onSelect(Contact contact) {

        Conversation conversation = UserInfo.database.conversationModel().getConversation(contact.UserID);
        if (conversation == null) {
            conversation = new Conversation(contact.UserID);
            UserInfo.database.conversationModel().insertConversation(conversation);
        }
        ChatActivity.isGroupChat = false;
        ChatActivity.uniqueID = conversation.ConversationID;
        Intent intent = new Intent(ContactActivity.this, ChatActivity.class);
        startActivity(intent);
        finish();
    }
}
