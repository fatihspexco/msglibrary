package com.spexco.msgframework.ui.main.adapters;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.spexco.msgframework.MessageApplication;
import com.spexco.msgframework.db.BaseConversation;
import com.spexco.msgframework.db.Conversation;
import com.spexco.msgframework.db.Group;
import com.spexco.msgframework.db.GroupContactWriting;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.utils.Utility;
import com.spexco.msgframework.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by fatihinan on 04.12.2018.
 */
public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ViewHolder> {

    private List<BaseConversation> conversations = new ArrayList<>();
    private List<BaseConversation> filteredConversations = new ArrayList<>();
    private final ConversationListListener mListener;
    private final LifecycleOwner mLifecycleOwner;

    public ConversationAdapter(LifecycleOwner lifecycleOwner, ConversationListListener listener) {
        mLifecycleOwner = lifecycleOwner;
        mListener = listener;
    }

    public void setConversations(List<BaseConversation> conversations) {
        this.conversations = conversations;
        filteredConversations.addAll(conversations);
    }

    public void filter(String name) {
        filteredConversations.clear();

        if (name.length() > 0) {

            for (BaseConversation b : conversations) {

                if (b instanceof Conversation) {
                    Conversation c = (Conversation) b;
                    if (c.VisibleName.toLowerCase().contains(name.toLowerCase())) {
                        filteredConversations.add(c);
                    }
                } else if (b instanceof Group) {
                    Group g = (Group) b;
                    if (g.GroupName.toLowerCase().contains(name.toLowerCase())) {
                        filteredConversations.add(g);
                    }
                }


            }
        } else {
            filteredConversations.addAll(conversations);
        }

        MessageApplication.mainHandler.post(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    public List<BaseConversation> getConversations() {
        return filteredConversations;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_conversation, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = filteredConversations.get(position);

        //Log.e("testtest", "onBindViewHolder conv");

        Message message = holder.mItem.lastMessage;
        holder.updateLastMessage(message);

        int unreadMessagesCount = 0;

        if (holder.mItem instanceof Conversation) {

            Conversation c = (Conversation) holder.mItem;
            holder.mVisibleNameView.setText(c.VisibleName);

            unreadMessagesCount = UserInfo.database.messageModel().getUnreadMessagesCountInConversation(UserInfo.myUserID, c.ConversationID);

            //Log.e("testtest", c.ConversationID + "-" + "conversation onBindViewHolder");

            if (holder.liveDataWriting != null) {
                holder.liveDataWriting.removeObservers(mLifecycleOwner);
            }
            holder.liveDataWriting  = UserInfo.database.conversationModel().getConversationWriting(c.ContactID);
            holder.liveDataWriting.observe(mLifecycleOwner, new Observer<Boolean>() {
                @Override
                public void onChanged(@Nullable Boolean isWriting) {
                    if (isWriting != null && isWriting) {
                        holder.mLastMessageView.setText("yazıyor...");
                    } else if (holder.lastMessage != null) {
                        holder.mLastMessageView.setText(holder.lastMessage.getNotificationText());
                    } else {
                        holder.mLastMessageView.setText("");
                    }
                }
            });

            if ( Utility.isValid(c.PhotoURL) ) {
                Glide
                        .with(holder.mProfileImageView)
                        .load(c.PhotoURL)
                        .into(holder.mProfileImageView);
            } else {
                Glide
                        .with(holder.mProfileImageView)
                        .load(R.drawable.ic_default_user)
                        .into(holder.mProfileImageView);
            }

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mListener) {
                        mListener.onSelect(holder.mItem);
                    }
                }
            });

            holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mListener.onLongSelect(holder.mItem, holder.mLastMessageTimeView);
                    return true;
                }
            });
        }

        else if (holder.mItem instanceof Group) {

            Group g = (Group) holder.mItem;

            holder.mVisibleNameView.setText(g.GroupName);

            unreadMessagesCount = UserInfo.database.messageModel().getUnreadMessagesCountInGroup(UserInfo.myUserID, g.GroupID);

            //Log.e("testtest", g.GroupID + "-" + "group onBindViewHolder");

            if (holder.liveDataWriting != null) {
                holder.liveDataWriting.removeObservers(mLifecycleOwner);
            }
            holder.liveDataWriting = UserInfo.database.conversationModel().getGroupConversationWriting(g.GroupID);
            holder.liveDataWriting.observe(mLifecycleOwner, new Observer<GroupContactWriting>() {
                @Override
                public void onChanged(@Nullable GroupContactWriting groupContactWriting) {
                    if (groupContactWriting != null && groupContactWriting.isWriting) {
                        holder.mLastMessageView.setText(UserInfo.database.contactModel().getContactVisibleName(groupContactWriting.ContactID) + " yazıyor...");
                    } else if (holder.lastMessage != null) {
                        holder.mLastMessageView.setText(holder.lastMessage.getNotificationText());
                    } else  {
                        holder.mLastMessageView.setText("");
                    }
                }
            });

            if ( Utility.isValid(g.PhotoURL) ) {
                Glide
                        .with(holder.mProfileImageView)
                        .load(g.PhotoURL)
                        .into(holder.mProfileImageView);
            } else {
                Glide
                        .with(holder.mProfileImageView)
                        .load(R.drawable.ic_default_group)
                        .into(holder.mProfileImageView);
            }

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mListener) {
                        mListener.onSelect(holder.mItem);
                    }
                }
            });

            holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    mListener.onLongSelect(holder.mItem, holder.mLastMessageTimeView);
                    return true;
                }
            });
        }

        if (unreadMessagesCount > 0) {
            holder.mUnreadMessagesCountView.setText(unreadMessagesCount > 99 ? "99+" : String.valueOf(unreadMessagesCount));
            holder.mUnreadMessagesCountView.setVisibility(View.VISIBLE);
        } else {
            holder.mUnreadMessagesCountView.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return filteredConversations != null ? filteredConversations.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mVisibleNameView;
        public final TextView mLastMessageView;
        public final TextView mLastMessageTimeView;
        public final TextView mUnreadMessagesCountView;
        public final CircleImageView mProfileImageView;
        public BaseConversation mItem;
        public Message lastMessage;
        public LiveData liveDataWriting;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mVisibleNameView = view.findViewById(R.id.tv_visible_name);
            mLastMessageView = view.findViewById(R.id.tv_last_message);
            mLastMessageTimeView = view.findViewById(R.id.tv_last_message_time);
            mUnreadMessagesCountView = view.findViewById(R.id.tv_unread_message_count);
            mProfileImageView = view.findViewById(R.id.iv_profile_image);
        }

        public void updateLastMessage(Message message) {
            lastMessage = message;
            if (lastMessage != null) {
                mLastMessageView.setText(lastMessage.getNotificationText());
                mLastMessageTimeView.setText(Utility.getTimeAgo(lastMessage.CreateDate));
            } else {
                mLastMessageView.setText("");
                mLastMessageTimeView.setText("");
            }
        }
    }

    public interface ConversationListListener {
        void onSelect(BaseConversation conversation);
        void onLongSelect(BaseConversation conversation, View view);
    }
}
