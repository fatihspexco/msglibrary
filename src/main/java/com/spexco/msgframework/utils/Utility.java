package com.spexco.msgframework.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.provider.ContactsContract;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.spexco.msgframework.db.AppDatabase;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.socket.ResultParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import android.provider.ContactsContract.CommonDataKinds.*;

import static com.spexco.msgframework.utils.Consts.*;

/**
 * Created by fatihinan on 4.12.2018.
 */
public class Utility {

    public static boolean isValid(String str) {
        return str != null && str.length() > 0;
    }

    public static String getRandomID() {
        return UUID.randomUUID().toString();
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static void saveString(String key, String value) {
        SharedPreferences sharedPref = AppDatabase.context.getSharedPreferences("sp_spexmesaj", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getString(String key) {
        SharedPreferences sharedPref = AppDatabase.context.getSharedPreferences("sp_spexmesaj", Context.MODE_PRIVATE);
        return sharedPref.getString(key, "");
    }

    public static String getString(String key, String defaultValue) {
        SharedPreferences sharedPref = AppDatabase.context.getSharedPreferences("sp_spexmesaj", Context.MODE_PRIVATE);
        return sharedPref.getString(key, defaultValue);
    }

    public static String getRoleText(int role) {

        switch (role) {
            case PARTICIPANT_ROLE_OWNER:
                return "Sahibi";
            case PARTICIPANT_ROLE_ADMIN:
                return "Yönetici";
            default:
                return "";
        }
    }

    public static String hashKeyForDisk(String key) {
        String cacheKey;
        try {
            final MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(key.getBytes());
            cacheKey = bytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            cacheKey = String.valueOf(key.hashCode());
        }
        return cacheKey;
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static String convertDateToString(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        try {
            return sdf.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getTimeAgo(Date date) {

        try {
            long time = date.getTime();
            int hourDiff = getTimeDistanceInMinutes(time);

            String timeAgo = "";

            if (hourDiff < 24) {
                timeAgo = convertDateToString(date, "HH:mm");
            } else if (hourDiff < 48) {
                timeAgo = "Dün";
            } else {
                timeAgo = convertDateToString(date, "dd MMM yyyy");
            }

            return timeAgo;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = Calendar.getInstance().getTime().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60 / 60);
    }

    public static void sendWritingInfo(String contactID, String task, boolean isGroup) {
        JsonObject messageObject = new JsonObject();
        messageObject.addProperty(KEY_TYPE, VALUE_TYPE_SET);
        messageObject.addProperty(KEY_TASK, task);
        messageObject.addProperty(KEY_FROM, UserInfo.myUserID);
        messageObject.addProperty(KEY_TO, contactID);
        messageObject.addProperty(KEY_IS_GROUP, isGroup);

        UserInfo.sendToSocket(messageObject.toString());
    }

    public static void updateMessage(Message message) {
        JsonObject messageObject = new JsonObject();
        messageObject.addProperty(KEY_TYPE, VALUE_TYPE_SET);
        messageObject.addProperty(KEY_TASK, VALUE_TASK_UPDATE_MESSAGE);
        messageObject.addProperty(KEY_FROM, UserInfo.myUserID);
        messageObject.add(VALUE_TYPE_MESSAGE, UserInfo.gson.toJsonTree(message, Message.class));

        if (!UserInfo.sendToSocket(messageObject.toString())) {
            message.isSync = false;
            UserInfo.database.messageModel().insertMessage(message);
        }
    }

    public static void updateConversation(List<ResultParser.MessageStatusInfo> info) {
        JsonObject messageObject = new JsonObject();
        messageObject.addProperty(KEY_TYPE, VALUE_TYPE_SET);
        messageObject.addProperty(KEY_TASK, VALUE_TASK_UPDATE_CONVERSATION);
        messageObject.addProperty(KEY_FROM, UserInfo.myUserID);

        JsonArray conversationsArray = new JsonArray();
        for (ResultParser.MessageStatusInfo msi : info) {
            JsonElement jsonElement = UserInfo.gson.toJsonTree(msi);
            JsonObject jsonObject = (JsonObject) jsonElement;
            conversationsArray.add(jsonObject);
        }
        messageObject.add(KEY_CONVERSATIONS, conversationsArray);

        UserInfo.sendToSocket(messageObject.toString());
    }

    public static void uploadFile(final UploadFileListener listener, final File file){

        String url = SERVICE_ADDRESS + "uploadfile";
        AndroidNetworking.upload(url)
                .addMultipartFile("file",file)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("testtest", response);
                        UploadResult result = UserInfo.gson.fromJson(response, UploadResult.class);
                        if (result.Success) {
                            listener.onResponse(true, result.URL);
                        } else {
                            listener.onResponse(false, result.Error);
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        listener.onResponse(false, error.toString());
                    }
                });
    }


    public class UploadResult {
        @Expose
        public boolean Success;

        @Expose
        public String URL;

        @Expose
        public String Error;
    }

    public interface UploadFileListener {
        void onResponse(boolean success, String description);
    }

    public static String getInfoMessageText(String body) {

        String text = "";
        try {
            JSONObject json = new JSONObject(body);
            int type = json.getInt(KEY_TYPE);

            switch (type) {
                case INFO_TYPE_CREATE_GROUP: {
                    String from = json.getString(KEY_FROM);
                    String groupName = json.getString(KEY_GROUP_NAME);
                    String visibleName = UserInfo.database.contactModel().getContactVisibleName(from);
                    if (from.contentEquals(UserInfo.myUserID)) {
                        text = "\"" + groupName + "\" grubununu oluşturdunuz";
                    } else if (isValid(visibleName) && !visibleName.contentEquals("null")){
                        text = visibleName + ", \"" + groupName + "\" grubunu oluşturdu";
                    } else {
                        text = "\"" + groupName + "\" grubunu oluşturuldu";
                    }
                    break;
                }
                case INFO_TYPE_REMOVE_PARTICIPANT:
                case INFO_TYPE_ADD_PARTICIPANT: {
                    String from = json.getString(KEY_FROM);
                    String[] toArr = UserInfo.gson.fromJson(json.getString(KEY_TO), String[].class);
                    String to = "";
                    for (String str : toArr) {
                        if (str.contentEquals(UserInfo.myUserID)) {
                            to += "sizi" + ", ";
                        } else {
                            to += UserInfo.database.contactModel().getContactVisibleName(str) + ", ";
                        }
                    }
                    if (to.length() >= 2) {
                        to = to.substring(0, to.length()-2);
                    }
                    boolean endsWithYou = to.endsWith("sizi");
                    String fromVisibleName = UserInfo.database.contactModel().getContactVisibleName(from);

                    if (!isValid(fromVisibleName) || fromVisibleName.contentEquals("null")) {
                        fromVisibleName = "";
                    }

                    if (from.contentEquals(UserInfo.myUserID)) {
                        text = to +
                                (endsWithYou ? " " : (toArr.length > 1 ? " kişilerini " : " kişisini ")) +
                                (type == INFO_TYPE_REMOVE_PARTICIPANT ? "çıkardınız" : "eklediniz");
                    } else {
                        if (isValid(fromVisibleName)) {
                            text = fromVisibleName + ", " + to +
                                    (endsWithYou ? " " : (toArr.length > 1 ? " kişilerini " : " kişisini ")) +
                                    (type == INFO_TYPE_REMOVE_PARTICIPANT ? "çıkardı" : "ekledi");
                        } else {
                            if (endsWithYou) {
                                text = type == INFO_TYPE_REMOVE_PARTICIPANT ? "çıkarıldınız" : "eklendiniz";
                            } else {
                                text = to + (endsWithYou ? " " : (toArr.length > 1 ? " kişileri " : " kişisi ")) +
                                        (type == INFO_TYPE_REMOVE_PARTICIPANT ? "çıkarıldı" : "eklendi");
                            }

                        }
                    }

                    break;
                } case INFO_TYPE_LEAVE_GROUP: {
                    String from = json.getString(KEY_FROM);
                    String fromVisibleName = UserInfo.database.contactModel().getContactVisibleName(from);
                    if (from.contentEquals(UserInfo.myUserID)) {
                        text = "Gruptan ayrıldınız";
                    } else {
                        text = fromVisibleName + " gruptan ayrıldı";
                    }
                    break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }

    public static Intent getContactIntent(String body) {
        try {
            ArrayList<ContentValues> data = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(body);
            JSONArray phonesHomeArr = jsonObject.getJSONArray(KEY_PHONES_HOME);
            JSONArray phonesMobileArr = jsonObject.getJSONArray(KEY_PHONES_MOBILE);
            JSONArray phonesWorkArr = jsonObject.getJSONArray(KEY_PHONES_WORK);
            JSONArray phonesOtherArr = jsonObject.getJSONArray(KEY_PHONES_OTHER);
            JSONArray emailsHomeArr = jsonObject.getJSONArray(KEY_EMAILS_HOME);
            JSONArray emailsMobileArr = jsonObject.getJSONArray(KEY_EMAILS_MOBILE);
            JSONArray emailsWorkArr = jsonObject.getJSONArray(KEY_EMAILS_WORK);
            JSONArray emailsOtherArr = jsonObject.getJSONArray(KEY_EMAILS_OTHER);

            for (int i=0; i<phonesHomeArr.length(); i++) {
                ContentValues row = new ContentValues();
                row.put(ContactsContract.RawContacts.Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
                row.put(ContactsContract.CommonDataKinds.Phone.TYPE, Phone.TYPE_HOME);
                row.put(Phone.NUMBER, phonesHomeArr.getString(i));
                data.add(row);
            }
            for (int i=0; i<phonesMobileArr.length(); i++) {
                ContentValues row = new ContentValues();
                row.put(ContactsContract.RawContacts.Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
                row.put(ContactsContract.CommonDataKinds.Phone.TYPE, Phone.TYPE_MOBILE);
                row.put(Phone.NUMBER, phonesMobileArr.getString(i));
                data.add(row);
            }
            for (int i=0; i<phonesWorkArr.length(); i++) {
                ContentValues row = new ContentValues();
                row.put(ContactsContract.RawContacts.Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
                row.put(ContactsContract.CommonDataKinds.Phone.TYPE, Phone.TYPE_WORK);
                row.put(Phone.NUMBER, phonesWorkArr.getString(i));
                data.add(row);
            }
            for (int i=0; i<phonesOtherArr.length(); i++) {
                ContentValues row = new ContentValues();
                row.put(ContactsContract.RawContacts.Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE);
                row.put(ContactsContract.CommonDataKinds.Phone.TYPE, Phone.TYPE_OTHER);
                row.put(Phone.NUMBER, phonesOtherArr.getString(i));
                data.add(row);
            }
            for (int i=0; i<emailsHomeArr.length(); i++) {
                ContentValues row = new ContentValues();
                row.put(ContactsContract.RawContacts.Data.MIMETYPE, Email.CONTENT_ITEM_TYPE);
                row.put(ContactsContract.CommonDataKinds.Email.TYPE, Email.TYPE_HOME);
                row.put(Email.ADDRESS, emailsHomeArr.getString(i));
                data.add(row);
            }
            for (int i=0; i<emailsMobileArr.length(); i++) {
                ContentValues row = new ContentValues();
                row.put(ContactsContract.RawContacts.Data.MIMETYPE, Email.CONTENT_ITEM_TYPE);
                row.put(ContactsContract.CommonDataKinds.Email.TYPE, Email.TYPE_MOBILE);
                row.put(Email.ADDRESS, emailsMobileArr.getString(i));
                data.add(row);
            }
            for (int i=0; i<emailsWorkArr.length(); i++) {
                ContentValues row = new ContentValues();
                row.put(ContactsContract.RawContacts.Data.MIMETYPE, Email.CONTENT_ITEM_TYPE);
                row.put(ContactsContract.CommonDataKinds.Email.TYPE, Email.TYPE_WORK);
                row.put(Email.ADDRESS, emailsWorkArr.getString(i));
                data.add(row);
            }
            for (int i=0; i<emailsOtherArr.length(); i++) {
                ContentValues row = new ContentValues();
                row.put(ContactsContract.RawContacts.Data.MIMETYPE, Email.CONTENT_ITEM_TYPE);
                row.put(ContactsContract.CommonDataKinds.Email.TYPE, Email.TYPE_OTHER);
                row.put(Email.ADDRESS, emailsOtherArr.getString(i));
                data.add(row);
            }

            Intent intent = new Intent(Intent.ACTION_INSERT, ContactsContract.Contacts.CONTENT_URI);
            intent.putParcelableArrayListExtra(ContactsContract.Intents.Insert.DATA, data);
            intent.putExtra(ContactsContract.Intents.Insert.NAME, jsonObject.getString(KEY_DISPLAY_NAME));
            return intent;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getContactInfo(String body) {
        try {
            JSONObject jsonObject = new JSONObject(body);
            JSONArray phonesHomeArr = jsonObject.getJSONArray(KEY_PHONES_HOME);
            JSONArray phonesMobileArr = jsonObject.getJSONArray(KEY_PHONES_MOBILE);
            JSONArray phonesWorkArr = jsonObject.getJSONArray(KEY_PHONES_WORK);
            JSONArray phonesOtherArr = jsonObject.getJSONArray(KEY_PHONES_OTHER);
            JSONArray emailsHomeArr = jsonObject.getJSONArray(KEY_EMAILS_HOME);
            JSONArray emailsMobileArr = jsonObject.getJSONArray(KEY_EMAILS_MOBILE);
            JSONArray emailsWorkArr = jsonObject.getJSONArray(KEY_EMAILS_WORK);
            JSONArray emailsOtherArr = jsonObject.getJSONArray(KEY_EMAILS_OTHER);
            StringBuilder text = new StringBuilder();
            text.append(jsonObject.getString(KEY_DISPLAY_NAME)).append("\n");
            for (int i=0; i<phonesHomeArr.length(); i++) {
                text.append(phonesHomeArr.getString(i)).append("\n");
            }
            for (int i=0; i<phonesMobileArr.length(); i++) {
                text.append(phonesMobileArr.getString(i)).append("\n");
            }
            for (int i=0; i<phonesWorkArr.length(); i++) {
                text.append(phonesWorkArr.getString(i)).append("\n");
            }
            for (int i=0; i<phonesOtherArr.length(); i++) {
                text.append(phonesOtherArr.getString(i)).append("\n");
            }
            for (int i=0; i<emailsHomeArr.length(); i++) {
                text.append(emailsHomeArr.getString(i)).append("\n");
            }
            for (int i=0; i<emailsMobileArr.length(); i++) {
                text.append(emailsMobileArr.getString(i)).append("\n");
            }
            for (int i=0; i<emailsWorkArr.length(); i++) {
                text.append(emailsWorkArr.getString(i)).append("\n");
            }
            for (int i=0; i<emailsOtherArr.length(); i++) {
                text.append(emailsOtherArr.getString(i)).append("\n");
            }
            if (text.length() >= 2) {
                return text.substring(0, text.length()-1);
            }
            return text.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
