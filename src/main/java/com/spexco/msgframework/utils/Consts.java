package com.spexco.msgframework.utils;

import android.os.Environment;

/**
 * Created by fatihinan on 3.12.2018.
 */
public class Consts {

    public static String WS_ADDRESS = "";
    public static String SERVICE_ADDRESS = "";

    public static final String LOCAL_FILE_DIR = Environment.getExternalStorageDirectory() + "/spec/";

    //conversation history
    public static final String KEY_FROM_DATE = "FromDate";
    public static final String KEY_UNIQUE_ID = "UniqueID";

    public static final String KEY_TYPE = "Type";
    public static final String VALUE_TYPE_MESSAGE = "message";
    public static final String VALUE_TYPE_GET = "get";
    public static final String VALUE_TYPE_SET = "set";
    public static final String VALUE_TYPE_RESULT = "result";
    public static final String VALUE_TYPE_INFO = "info";

    public static final String KEY_TASK = "Task";
    public static final String KEY_SUCCESS = "Success";
    public static final String VALUE_TASK_LOGIN = "login";
    public static final String VALUE_TASK_UPDATED_DATA = "updated_data";
    public static final String VALUE_TASK_CONVERSATION_HISTORY = "conversation_history";
    public static final String VALUE_TASK_UPDATE_PROFILE = "update_profile";
    public static final String VALUE_TASK_REGISTER_PUSH_TOKEN = "register_push_token";
    public static final String VALUE_TASK_START_TYPING = "start_typing";
    public static final String VALUE_TASK_STOP_TYPING = "stop_typing";
    public static final String VALUE_TASK_UPDATE_MESSAGE = "update_message";
    public static final String VALUE_TASK_UPDATE_CONVERSATION = "update_conversation";
    public static final String VALUE_TASK_UPDATE_MESSAGE_STATUS = "update_message_status";
    public static final String VALUE_TASK_CREATE_GROUP = "create_group";
    public static final String VALUE_TASK_UPDATE_GROUP = "update_group";
    public static final String VALUE_TASK_GROUP_ADD_PARTICIPANT = "group_add_participant";
    public static final String VALUE_TASK_GROUP_REMOVE_PARTICIPANT = "group_remove_participant";
    public static final String VALUE_TASK_GROUP_LEAVE = "group_leave";
    public static final String VALUE_TASK_MAKE_ADMIN = "group_make_admin";
    public static final String VALUE_TASK_REMOVE_ADMIN = "group_remove_admin";
    public static final String VALUE_TASK_MAKE_OWNER = "group_make_owner";

    public static final int MESSAGE_STATUS_WAITING = 0;
    public static final int MESSAGE_STATUS_SEND = 1;
    public static final int MESSAGE_STATUS_DELIVERED = 2;
    public static final int MESSAGE_STATUS_SEEN = 3;
    public static final int MESSAGE_STATUS_DENY = 4;
    public static final int MESSAGE_STATUS_ERROR = 6;
    public static final int MESSAGE_STATUS_DELETED_ME = 11;
    public static final int MESSAGE_STATUS_DELETED_EVERYONE = 12;

    //message types
    public static final int MESSAGE_TYPE_TEXT = 0;
    public static final int MESSAGE_TYPE_PICTURE = 1;
    public static final int MESSAGE_TYPE_VIDEO = 2;
    public static final int MESSAGE_TYPE_LINK = 3;
    public static final int MESSAGE_TYPE_LOCATION = 4;
    public static final int MESSAGE_TYPE_VOICE = 5;
    public static final int MESSAGE_TYPE_CONTACT = 6;
    public static final int MESSAGE_TYPE_INFO = 7;
    public static final int MESSAGE_TYPE_OTHER = 100;

    //info message types
    public static final int INFO_TYPE_CREATE_GROUP = 0;
    public static final int INFO_TYPE_ADD_PARTICIPANT = 1;
    public static final int INFO_TYPE_REMOVE_PARTICIPANT = 2;
    public static final int INFO_TYPE_LEAVE_GROUP = 3;

    //group participant roles
    public static final int PARTICIPANT_ROLE_OWNER = 0;
    public static final int PARTICIPANT_ROLE_ADMIN = 1;
    public static final int PARTICIPANT_ROLE_DEFAULT = 2;
    public static final int PARTICIPANT_ROLE_DELETED = 3;

    public static final String KEY_MESSAGE_ID = "MessageID";
    public static final String KEY_FROM = "From";
    public static final String KEY_TO = "To";
    public static final String KEY_MESSAGE_TYPE = "MessageType";
    public static final String KEY_BODY = "Body";
    public static final String KEY_CONVERSATION_ID = "ConversationID";
    public static final String KEY_MEDIA_URL = "MediaURL";
    public static final String KEY_STATUS = "Status";
    public static final String KEY_CREATE_DATE = "CreateDate";
    public static final String KEY_UPDATE_DATE = "UpdateDate";
    public static final String KEY_DELIVER_DATE = "DeliverDate";
    public static final String KEY_SEEN_DATE = "SeenDate";
    public static final String KEY_IS_GROUP = "isGroup";

    public static final String KEY_INIT_DATA = "InitData";

    //update
    public static final String KEY_DEVICE_TYPE = "DeviceType";
    public static final String KEY_VERSION = "Version";
    public static final String KEY_LINK = "Link";

    public static final String KEY_USER_ID = "UserID";
    public static final String KEY_VISIBLE_NAME = "VisibleName";
    public static final String KEY_PHOTO_URL = "PhotoURL";
    public static final String KEY_STATUS_TEXT = "StatusText";
    public static final String KEY_PHONE_NUMBER = "PhoneNumber";
    public static final String KEY_EMAIL_ADDRESS = "EmailAddress";
    public static final String KEY_ROLE = "Role";
    public static final String KEY_PUSH_TOKEN = "PushToken";

    public static final String KEY_GROUP = "Group";
    public static final String KEY_GROUP_ID = "GroupID";
    public static final String KEY_GROUP_NAME = "GroupName";
    public static final String KEY_GROUP_DESCRIPTION = "GroupDescription";
    public static final String KEY_PARTICIPANTS = "Participants";
    public static final String KEY_OWNER = "Owner";
    public static final String KEY_MESSAGE_AUTH = "MessageAuth";
    public static final String KEY_CONVERSATIONS = "Conversations";

    //message type location
    public static final String KEY_LAT = "lat";
    public static final String KEY_LNG = "lng";

    //message type contact
    public static final String KEY_DISPLAY_NAME = "display_name";
    public static final String KEY_PHONES_HOME = "phones_home";
    public static final String KEY_PHONES_MOBILE = "phones_mobile";
    public static final String KEY_PHONES_WORK = "phones_work";
    public static final String KEY_PHONES_OTHER = "phones_other";
    public static final String KEY_EMAILS_HOME = "emails_home";
    public static final String KEY_EMAILS_MOBILE = "emails_mobile";
    public static final String KEY_EMAILS_WORK = "emails_work";
    public static final String KEY_EMAILS_OTHER = "emails_other";

    //shared pref
    public static final String KEY_LOGIN_TOKEN = "login_token";

}
