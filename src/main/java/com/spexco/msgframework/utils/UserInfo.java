package com.spexco.msgframework.utils;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.spexco.msgframework.BuildConfig;
import com.spexco.msgframework.db.AppDatabase;
import com.spexco.msgframework.db.Contact;
import com.spexco.msgframework.socket.SpexMesajSocketClient;

import static com.spexco.msgframework.MessageApplication.getVersionName;
import static com.spexco.msgframework.utils.Consts.*;
import static com.spexco.msgframework.utils.Utility.getString;
import static com.spexco.msgframework.utils.Utility.isValid;
import static com.spexco.msgframework.utils.Utility.saveString;

/**
 * Created by fatihinan on 30.11.2018.
 */
public class UserInfo {

    public static String myUserID;
    public static String pushToken;
    public static Contact myContact;
    public static String source = "mobile";
    public static Context context;
    public static AppDatabase database;
    public static Gson gson;

    public static void registerPushToken() {
        if (isValid(myUserID) && isValid(pushToken)) {

            JsonObject messageObject = new JsonObject();
            messageObject.addProperty(KEY_TYPE, VALUE_TYPE_SET);
            messageObject.addProperty(KEY_TASK, VALUE_TASK_REGISTER_PUSH_TOKEN);
            messageObject.addProperty(KEY_FROM, myUserID);
            messageObject.addProperty(KEY_PUSH_TOKEN, pushToken);

            sendToSocket(messageObject.toString());
        }
    }

    public static String getUpdatedDataJsonString() {
        JsonObject messageObject = new JsonObject();
        messageObject.addProperty(KEY_TYPE, VALUE_TYPE_GET);
        messageObject.addProperty(KEY_TASK, VALUE_TASK_UPDATED_DATA);
        messageObject.addProperty(KEY_FROM, myUserID);
        messageObject.addProperty(KEY_VERSION, getVersionName());
        messageObject.addProperty(KEY_DEVICE_TYPE, 2);
        messageObject.addProperty(KEY_UPDATE_DATE, getString(KEY_UPDATE_DATE,"-1"));

        return messageObject.toString();
    }

    public static String getConversationHistory(java.util.Date from, String uniqueID) {
        JsonObject messageObject = new JsonObject();
        messageObject.addProperty(KEY_TYPE, VALUE_TYPE_GET);
        messageObject.addProperty(KEY_TASK, VALUE_TASK_CONVERSATION_HISTORY);
        messageObject.addProperty(KEY_FROM, myUserID);
        messageObject.addProperty(KEY_FROM_DATE, GsonDateFormatAdapter.sdf.format(from));
        messageObject.addProperty(KEY_UNIQUE_ID, uniqueID);

        return messageObject.toString();
    }

    public static void saveUser() {
        database.contactModel().insertContact(myContact);

        JsonObject messageObject = new JsonObject();
        messageObject.addProperty(KEY_TYPE, VALUE_TYPE_SET);
        messageObject.addProperty(KEY_TASK, VALUE_TASK_UPDATE_PROFILE);
        messageObject.addProperty(KEY_FROM, myContact.UserID);
        messageObject.addProperty(KEY_VISIBLE_NAME, myContact.VisibleName);
        messageObject.addProperty(KEY_PHOTO_URL, myContact.PhotoURL);
        messageObject.addProperty(KEY_STATUS_TEXT, myContact.StatusText);

        sendToSocket(messageObject.toString());
    }

    public static void updateGroup(String groupID, String photoURL, String groupName, String groupDescription, int messageAuth) {
        JsonObject messageObject = new JsonObject();
        messageObject.addProperty(KEY_TYPE, VALUE_TYPE_SET);
        messageObject.addProperty(KEY_TASK, VALUE_TASK_UPDATE_GROUP);
        messageObject.addProperty(KEY_FROM, myContact.UserID);
        messageObject.addProperty(KEY_GROUP_ID, groupID);
        messageObject.addProperty(KEY_GROUP_NAME, groupName);
        messageObject.addProperty(KEY_PHOTO_URL, photoURL);
        messageObject.addProperty(KEY_GROUP_DESCRIPTION, groupDescription);
        messageObject.addProperty(KEY_MESSAGE_AUTH, messageAuth);

        sendToSocket(messageObject.toString());
    }

    public static boolean sendToSocket(String message) {
        Log.e("testtest send -> ", message);
        return SpexMesajSocketClient.getInstance().send(message);
    }

    public static void setUserID(String userID) {
        saveString(KEY_USER_ID, userID);
        if ( isValid(userID) ) {
            myUserID = userID;
            myContact = database.contactModel().getContactById(userID);
        }
    }

    public static String getUserID() {
        if ( !isValid(myUserID) ) {
            myUserID = getString(KEY_USER_ID);
        }
        return myUserID;
    }

}
