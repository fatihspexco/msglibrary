package com.spexco.msgframework.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.ArrayList;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by fatihinan on 4.12.2018.
 */

@Dao
public interface ContactDao {

    @Query("SELECT * FROM contact WHERE contact.UserID != :except ORDER BY VisibleName COLLATE NOCASE")
    LiveData<List<Contact>> getAllContactsLive(String except);

    @Query("SELECT * FROM contact WHERE contact.UserID != :except ORDER BY VisibleName COLLATE NOCASE")
    List<Contact> getAllContacts(String except);

    @Query("SELECT * FROM contact WHERE UserID = :userID")
    Contact getContactById(String userID);

    @Query("SELECT VisibleName FROM contact WHERE UserID = :userID")
    String getContactVisibleName(String userID);

    @Query("SELECT StatusText FROM contact WHERE UserID = :userID")
    String getContactStatusText(String userID);

    @Insert(onConflict = REPLACE)
    void insertContact(ArrayList<Contact> contacts);

    @Insert(onConflict = REPLACE)
    void insertContact(Contact contact);
}
