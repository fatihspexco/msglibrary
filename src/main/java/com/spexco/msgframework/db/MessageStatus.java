package com.spexco.msgframework.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.spexco.msgframework.db.converters.DateConverter;

import java.util.Date;

/**
 * Created by fatihinan on 9.1.2019.
 */

@Entity
@TypeConverters(DateConverter.class)
public class MessageStatus {

    @PrimaryKey
    @NonNull
    @Expose(serialize = false, deserialize = false)
    public String PK;

    @Expose
    public String MessageID;

    @Expose
    public String GroupID;

    @Expose
    public String UserID;

    @Expose
    public int Status;

    @Expose
    public Date DeliverDate;

    @Expose
    public Date SeenDate;

    public MessageStatus() {

    }

    public MessageStatus(String messageID, String userID, int status, Date deliverDate, Date seenDate, String groupID) {
        MessageID = messageID;
        UserID = userID;
        Status = status;
        DeliverDate = deliverDate;
        SeenDate = seenDate;
        GroupID = groupID;
    }

    public void initPK() {
        PK = MessageID + UserID;
    }
}
