package com.spexco.msgframework.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.utils.Utility;
import com.spexco.msgframework.db.converters.DateConverter;

import java.util.Date;

/**
 * Created by fatihinan on 4.12.2018.
 */

@Entity
@TypeConverters(DateConverter.class)
public class Conversation extends BaseConversation {

    @PrimaryKey
    @NonNull
    @Expose
    public String ContactID;

    @Expose
    public String ConversationID;

    @Expose
    public Date CreateDate;

    @Expose
    public int Status;

    @Expose
    public String PhotoURL;

    @Expose(serialize = false, deserialize = false)
    public String VisibleName;

    public Conversation() {
        isDeleted = false;
    }

    public Conversation(@NonNull String contactID) {
        if (UserInfo.myUserID.compareTo(contactID) < 0) {
            ConversationID = Utility.hashKeyForDisk(UserInfo.myUserID + contactID);
        } else {
            ConversationID = Utility.hashKeyForDisk(contactID + UserInfo.myUserID);
        }
        ContactID = contactID;
        CreateDate = new Date();
        Status = 0;
    }

    public Conversation(@NonNull String contactID, String conversationID, Date createDate) {
        ConversationID = conversationID;
        ContactID = contactID;
        CreateDate = createDate;
        Status = 0;
    }
}

