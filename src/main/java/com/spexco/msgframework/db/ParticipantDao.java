package com.spexco.msgframework.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.ArrayList;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by fatihinan on 4.12.2018.
 */

@Dao
public interface ParticipantDao {

    @Query("SELECT * FROM Participant WHERE GroupID = :groupID AND Role!=3")
    LiveData<List<Participant>> getLiveParticipants(String groupID);

    @Query("SELECT * FROM Participant WHERE GroupID = :groupID AND Role!=3")
    List<Participant> getParticipants(String groupID);

    @Insert(onConflict = REPLACE)
    void insertParticipant(ArrayList<Participant> participants);

    @Insert(onConflict = REPLACE)
    void insertParticipant(Participant participant);

    @Query("DELETE FROM Participant WHERE GroupID = :groupID")
    void deleteParticipants(String groupID);

    @Query("SELECT * FROM Participant WHERE GroupID = :groupID AND UserID = :userID")
    Participant getParticipant(String groupID, String userID);
}
