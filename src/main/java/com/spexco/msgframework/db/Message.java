package com.spexco.msgframework.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.spexco.msgframework.R;
import com.spexco.msgframework.utils.Consts;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.utils.Utility;
import com.spexco.msgframework.db.converters.DateConverter;
import com.spexco.msgframework.db.converters.ListConverter;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by fatihinan on 30.11.2018.
 */

@Entity
@TypeConverters({DateConverter.class, ListConverter.class})
public class Message {

    @Ignore
    @Expose
    public String Type = "message";

    @PrimaryKey
    @NonNull
    @Expose
    public String MessageID;

    @Expose
    public String From;

    @Expose
    public String To;

    @Expose
    public int MessageType;

    @Expose
    public String Body;

    @Expose
    public String ConversationID;

    @Expose
    public ArrayList<String> MediaURLs;

    @Expose
    public int Status;

    @Expose
    public Date CreateDate;

    @Expose
    public Date DeliverDate;

    @Expose
    public Date SeenDate;

    @Expose
    public Date UpdateDate;

    @Expose
    public Boolean isGroup;

    @Expose(serialize = false, deserialize = false)
    public boolean isSync;

    public Message() {
        isSync = true;
    }

    public Message(String to, int messageType, String body, String conversationID, boolean isGroup) {

        this.MessageID = Utility.getRandomID();
        this.From = UserInfo.myContact.UserID;
        this.To = to;
        this.MessageType = messageType;
        this.Body = body;
        this.CreateDate = new Date();
        this.UpdateDate = CreateDate;
        this.ConversationID = conversationID;
        this.isGroup = isGroup;
        this.Status = Consts.MESSAGE_STATUS_WAITING;
        this.isSync = false;
    }

    public Message(Message msg) {
        MessageID = Utility.getRandomID();
        From = msg.From;
        To = msg.To;
        MessageType = msg.MessageType;
        Body = msg.Body;
        ConversationID = msg.ConversationID;
        MediaURLs = msg.MediaURLs;
        Status = Consts.MESSAGE_STATUS_WAITING;
        CreateDate = new Date();
        DeliverDate = CreateDate;
        isGroup = msg.isGroup;
    }

    @Ignore
    @Expose(serialize = false, deserialize = false)
    private int resources[] = new int[] {R.drawable.ic_message_status_waiting,
            R.drawable.ic_message_status_sent,
            R.drawable.ic_message_status_delivered,
            R.drawable.ic_message_status_seen
    };

    public int getMessageStatusResource() {
        if (Status >= 0 && Status <= 4) {
            return resources[Status];
        }
        return R.drawable.ic_message_status_unknown;
    }

    public String getNotificationText() {
        if (MessageType == Consts.MESSAGE_TYPE_LOCATION) {
            return "Konum";
        } else if (MessageType == Consts.MESSAGE_TYPE_CONTACT) {
            return "Kişi";
        } else if (MessageType == Consts.MESSAGE_TYPE_PICTURE){
            return "Resim" + (Body.length() > 0 ? ": " + Body : "");
        } else if (MessageType == Consts.MESSAGE_TYPE_VIDEO){
            return "Video: " + (Body.length() > 0 ? ": " + Body : "");
        } else if (MessageType == Consts.MESSAGE_TYPE_INFO){
            return Utility.getInfoMessageText(Body);
        } else {
            return Body;
        }
    }
}
