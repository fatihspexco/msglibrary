package com.spexco.msgframework.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import com.spexco.msgframework.db.converters.DateConverter;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by fatihinan on 9.1.2019.
 */

@Dao
@TypeConverters(DateConverter.class)
public interface MessageStatusDao {

    @Query("SELECT * FROM MessageStatus WHERE MessageID = :messageID")
    List<MessageStatus> getMessageStatus(String messageID);

    @Query("SELECT Status FROM MessageStatus WHERE MessageID = :messageID AND UserID = :userID")
    int getMessageStatus(String messageID, String userID);

    @Insert(onConflict = REPLACE)
    void insertMessageStatus(ArrayList<MessageStatus> messageStatus);

    @Insert(onConflict = REPLACE)
    void insertMessageStatus(MessageStatus messageStatus);

    @Query("SELECT MessageID FROM MessageStatus WHERE UserID = :userID AND GroupID = :groupID AND Status < :status")
    List<String> userGetMessageStatus(String userID, String groupID, int status);

    @Query("UPDATE MessageStatus SET Status = :status, DeliverDate = :updateDate WHERE UserID = :userID AND GroupID = :groupID AND Status < :status")
    void userSetMessageStatusDelivered(String userID, String groupID, int status, Date updateDate);

    @Query("UPDATE MessageStatus SET Status = :status, SeenDate = :updateDate WHERE UserID = :userID AND GroupID = :groupID AND Status < :status")
    void userSetMessageStatusSeen(String userID, String groupID, int status, Date updateDate);
}
