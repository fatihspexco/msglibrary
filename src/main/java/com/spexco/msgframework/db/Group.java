package com.spexco.msgframework.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;

import static com.spexco.msgframework.utils.Utility.getRandomID;

/**
 * Created by fatihinan on 4.12.2018.
 */

@Entity
public class Group extends BaseConversation {

    @PrimaryKey
    @NonNull
    @Expose
    public String GroupID;

    @Expose
    public String GroupName;

    @Expose
    public String PhotoURL;

    @Expose
    public String GroupDescription;

    @Expose
    public String Owner;

    @Expose
    public int MessageAuth;

    @Expose
    public Boolean isLDAPGroup;

    @Expose
    public Boolean isActive;

    public Group() {
        GroupID = getRandomID();
    }
}
