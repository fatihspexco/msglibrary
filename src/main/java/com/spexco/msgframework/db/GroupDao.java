package com.spexco.msgframework.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import com.spexco.msgframework.db.converters.DateConverter;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by fatihinan on 4.12.2018.
 */

@TypeConverters(DateConverter.class)
@Dao
public interface GroupDao {

    @Query("SELECT * FROM `Group` WHERE isDeleted=0")
    LiveData<List<Group>> getAllGroups();

    @Query("SELECT * FROM `Group` WHERE GroupID = :groupID")
    LiveData<Group> getGroupWithInfo(String groupID);

    @Query("SELECT GroupName FROM `Group` WHERE GroupID = :groupID")
    String getGroupVisibleName(String groupID);

    @Insert(onConflict = REPLACE)
    void insertGroup(ArrayList<Group> groups);

    @Insert(onConflict = REPLACE)
    void insertGroup(Group group);

    @Query("UPDATE `Group` SET clearDate = :clearDate WHERE GroupID = :groupID")
    void setClearDate(Date clearDate, String groupID);

    @Query("UPDATE `Group` SET isDeleted = :isDeleted WHERE GroupID = :groupID")
    void setDeleted(boolean isDeleted, String groupID);
}
