package com.spexco.msgframework.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class GroupContactWriting {

    @PrimaryKey
    @NonNull
    public String GroupID;
    public String ContactID;
    public boolean isWriting;

    public GroupContactWriting(@NonNull String GroupID, String ContactID, boolean isWriting) {
        this.GroupID = GroupID;
        this.ContactID = ContactID;
        this.isWriting = isWriting;
    }
}
