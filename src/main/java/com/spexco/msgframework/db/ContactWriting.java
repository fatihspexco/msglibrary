package com.spexco.msgframework.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class ContactWriting {

    @PrimaryKey
    @NonNull
    public String ContactID;
    public boolean isWriting;

    public ContactWriting(@NonNull String ContactID, boolean isWriting) {
        this.ContactID = ContactID;
        this.isWriting = isWriting;
    }
}
