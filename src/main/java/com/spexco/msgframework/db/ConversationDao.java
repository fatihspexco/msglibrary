package com.spexco.msgframework.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import com.spexco.msgframework.db.converters.DateConverter;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by fatihinan on 4.12.2018.
 */

@Dao
@TypeConverters(DateConverter.class)
public interface ConversationDao {

    @Query("SELECT conversation.isDeleted, conversation.clearDate, conversation.Status, conversation.ContactID, conversation.ConversationID, contact.PhotoURL, contact.VisibleName FROM conversation " +
            "LEFT JOIN contact ON conversation.ContactID = contact.UserID WHERE isDeleted=0")
    LiveData<List<Conversation>> getAllConversations();

    @Query("SELECT isDeleted, conversation.clearDate, conversation.Status, conversation.ContactID, conversation.ConversationID, contact.PhotoURL, contact.VisibleName FROM conversation " +
            "LEFT JOIN contact ON conversation.ContactID = contact.UserID WHERE ConversationID = :conversationID")
    LiveData<Conversation> getConversationWithInfo(String conversationID);

    @Insert(onConflict = REPLACE)
    void setConversationWriting(ContactWriting contactWriting);

    @Query("SELECT isWriting FROM ContactWriting WHERE ContactID = :contactID")
    LiveData<Boolean> getConversationWriting(String contactID);

    @Query("UPDATE ContactWriting SET isWriting = 0")
    void initConversationWriting();

    @Insert(onConflict = REPLACE)
    void setGroupWriting(GroupContactWriting groupWriting);

    @Query("SELECT * FROM GroupContactWriting WHERE GroupID = :groupID")
    LiveData<GroupContactWriting> getGroupConversationWriting(String groupID);

    @Query("DELETE FROM GroupContactWriting")
    void initGroupConversationWriting();

    @Query("SELECT * FROM conversation WHERE ContactID = :contactID")
    Conversation getConversation(String contactID);

    @Insert(onConflict = IGNORE)
    void insertConversation(ArrayList<Conversation> conversations);

    @Insert(onConflict = IGNORE)
    void insertConversation(Conversation conversation);

    @Query("UPDATE Conversation SET clearDate = :clearDate WHERE ConversationID = :conversationID")
    void setClearDate(Date clearDate, String conversationID);

    @Query("UPDATE Conversation SET isDeleted = :isDeleted WHERE ConversationID = :conversationID")
    void setDeleted(boolean isDeleted, String conversationID);
}
