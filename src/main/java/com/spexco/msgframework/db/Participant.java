package com.spexco.msgframework.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;

/**
 * Created by fatihinan on 4.12.2018.
 */

@Entity
public class Participant {

    @PrimaryKey
    @NonNull
    @Expose(serialize = false, deserialize = false)
    public String PK;

    @Expose
    public String GroupID;

    @Expose
    public String UserID;

    @Expose
    public int Role;

    public Participant() {
    }

    public Participant(String groupID, String userID, int role) {
        GroupID = groupID;
        UserID = userID;
        Role = role;
        PK = GroupID + UserID;
    }
}
