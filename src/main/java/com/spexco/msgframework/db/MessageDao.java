package com.spexco.msgframework.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import com.spexco.msgframework.db.converters.DateConverter;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by fatihinan on 4.12.2018.
 */

@Dao
@TypeConverters(DateConverter.class)
public interface MessageDao {

    @Query("SELECT * FROM message WHERE ConversationID=:ConversationID AND CreateDate>:afterDate ORDER BY CreateDate DESC")
    List<Message> getMessagesInConversation(String ConversationID, Date afterDate);

    @Query("SELECT * FROM message WHERE ConversationID=:ConversationID ORDER BY CreateDate DESC")
    List<Message> getMessagesInConversation(String ConversationID);

    @Query("SELECT * FROM message WHERE `To`=:GroupID AND CreateDate>:afterDate ORDER BY CreateDate DESC")
    List<Message> getMessagesInGroupChat(String GroupID, Date afterDate);

    @Query("SELECT * FROM message WHERE `To`=:GroupID ORDER BY CreateDate DESC")
    List<Message> getMessagesInGroupChat(String GroupID);

    @Query("SELECT * FROM message WHERE ConversationID=:ConversationID ORDER BY CreateDate DESC LIMIT 1")
    Message getLastMessageInConversation(String ConversationID);

    @Query("SELECT * FROM message WHERE `To`=:GroupID ORDER BY CreateDate DESC LIMIT 1")
    Message getLastMessageInGroup(String GroupID);

    @Query("SELECT COUNT(*) FROM message WHERE `From`!=:UserID AND ConversationID=:ConversationID AND Status<3 AND MessageType!=7")
    Integer getUnreadMessagesCountInConversation(String UserID, String ConversationID);

    @Query("SELECT COUNT(*) FROM message WHERE `From`!=:UserID AND `To`=:GroupID AND Status<3 AND MessageType!=7")
    Integer getUnreadMessagesCountInGroup(String UserID, String GroupID);

    @Query("SELECT * FROM message WHERE isSync = 0")
    List<Message> getUnsyncMessages();

    @Query("SELECT * FROM message WHERE MessageID = :id")
    Message getMessageById(String id);

    @Query("SELECT * FROM message WHERE MessageID = :id")
    LiveData<Message> getLiveMessageById(String id);

    @Query("SELECT * FROM message WHERE Body = :b")
    Message getMessageByBody(String b);

    @Query("UPDATE Message SET Status = 3 WHERE MessageID = :messageID ")
    void setMessageSeen(String messageID);

    @Insert(onConflict = REPLACE)
    void insertMessage(ArrayList<Message> messages);

    @Insert(onConflict = REPLACE)
    void insertMessage(Message message);

    @Query("SELECT MessageID FROM Message WHERE `To` = :userID AND ConversationID = :conversationID AND Status < :status")
    List<String> userGetMessageStatus(String userID, String conversationID, int status);

    @Query("UPDATE Message SET Status = :status, DeliverDate = :updateDate, UpdateDate = :updateDate WHERE `To` = :userID AND ConversationID = :conversationID AND Status < :status")
    void userSetMessageStatusDelivered(String userID, String conversationID, int status, Date updateDate);

    @Query("UPDATE Message SET Status = :status, SeenDate = :updateDate, UpdateDate = :updateDate WHERE `To` = :userID AND ConversationID = :conversationID AND Status < :status")
    void userSetMessageStatusSeen(String userID, String conversationID, int status, Date updateDate);

    @Query("DELETE FROM Message WHERE ConversationID = :conversationID")
    void deleteMessagesInConversation(String conversationID);

    @Query("DELETE FROM Message WHERE `To` = :groupID")
    void deleteMessagesInGroup(String groupID);
}
