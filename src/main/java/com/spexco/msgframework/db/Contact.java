package com.spexco.msgframework.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.spexco.msgframework.utils.Consts;

/**
 * Created by fatihinan on 4.12.2018.
 */

@Entity
public class Contact {

    @PrimaryKey
    @NonNull
    @Expose
    public String UserID;

    @Expose
    public String VisibleName;

    @Expose
    public String PhotoURL;

    @Expose
    public String StatusText;

    @Expose
    public String PhoneNumber;

    @Expose
    public String EmailAddress;

    @Expose
    public String Role;

    @Expose
    public Boolean isActive;

    public Contact(String UserID, String VisibleName, String PhotoURL, String StatusText, String PhoneNumber, String EmailAddress, String Role) {
        this.UserID = UserID;
        this.VisibleName = VisibleName;
        this.PhotoURL = PhotoURL;
        this.StatusText = StatusText;
        this.PhoneNumber = PhoneNumber;
        this.EmailAddress = EmailAddress;
        this.Role = Role;
    }

    public Contact(JsonObject json) {

        this.UserID = json.get(Consts.KEY_USER_ID).getAsString();
        this.VisibleName = json.get(Consts.KEY_VISIBLE_NAME).getAsString();
        this.PhotoURL = json.get(Consts.KEY_PHOTO_URL).getAsString();
        this.StatusText = json.get(Consts.KEY_STATUS_TEXT).getAsString();
        this.PhoneNumber = json.get(Consts.KEY_PHONE_NUMBER).getAsString();
        this.EmailAddress = json.get(Consts.KEY_EMAIL_ADDRESS).getAsString();
        this.Role = json.get(Consts.KEY_ROLE).getAsString();
    }

    public String toJsonString() {
        JsonObject messageObject = new JsonObject();
        messageObject.addProperty(Consts.KEY_USER_ID, UserID);
        messageObject.addProperty(Consts.KEY_VISIBLE_NAME, VisibleName);
        messageObject.addProperty(Consts.KEY_PHOTO_URL, PhotoURL);
        messageObject.addProperty(Consts.KEY_STATUS_TEXT, StatusText);
        messageObject.addProperty(Consts.KEY_PHONE_NUMBER, PhoneNumber);
        messageObject.addProperty(Consts.KEY_EMAIL_ADDRESS, EmailAddress);
        messageObject.addProperty(Consts.KEY_ROLE, Role);

        return messageObject.toString();
    }
}
