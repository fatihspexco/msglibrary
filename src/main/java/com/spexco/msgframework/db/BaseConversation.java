package com.spexco.msgframework.db;


import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.TypeConverters;

import com.google.gson.annotations.Expose;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.db.converters.DateConverter;

import java.util.Date;


public class BaseConversation implements Comparable<BaseConversation> {

    @Expose(serialize = false, deserialize = false)
    @Ignore
    public Message lastMessage;

    @TypeConverters(DateConverter.class)
    @Expose(serialize = false, deserialize = false)
    public Date clearDate;

    @Expose(serialize = false, deserialize = false)
    public boolean isDeleted;

    @Override
    public int compareTo(BaseConversation c) {
        if (lastMessage != null && c.lastMessage != null) {
            return c.lastMessage.CreateDate.compareTo(lastMessage.CreateDate);
        } else if (lastMessage != null) {
            return -1;
        } else if (c.lastMessage != null) {
            return 1;
        }
        return 1;
    }

    public boolean isConversation() {
        return this instanceof Conversation;
    }

    public boolean isGroup() {
        return this instanceof Group;
    }

    public void clearMessages() {
        Date tmpDate = lastMessage != null ? lastMessage.CreateDate : null;
        if (isConversation()) {
            String conversationID = ((Conversation) this).ConversationID;
            UserInfo.database.messageModel().deleteMessagesInConversation(conversationID);
            if (tmpDate != null) {
                clearDate = tmpDate;
                UserInfo.database.conversationModel().setClearDate(clearDate, conversationID);
            }
        } else if (isGroup()) {
            String groupID = ((Group) this).GroupID;
            UserInfo.database.messageModel().deleteMessagesInGroup(groupID);
            if (tmpDate != null) {
                clearDate = tmpDate;
                UserInfo.database.groupModel().setClearDate(clearDate, groupID);
            }
        }
    }

    public void deleteConversation() {
        clearMessages();
        setDeleted(true);
    }

    public void setDeleted(boolean deleted) {
        if (isConversation()) {
            UserInfo.database.conversationModel().setDeleted(deleted, ((Conversation) this).ConversationID);
        } else if (isGroup()) {
            UserInfo.database.groupModel().setDeleted(deleted, ((Group) this).GroupID);
        }
    }
}
