package com.spexco.msgframework.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.spexco.msgframework.utils.UserInfo;

@Database(entities = {Contact.class, Message.class, MessageStatus.class, Conversation.class,
        ContactWriting.class, GroupContactWriting.class, Group.class, Participant.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract MessageStatusDao messageStatusModel();
    public abstract ParticipantDao participantModel();
    public abstract GroupDao groupModel();
    public abstract ContactDao contactModel();
    public abstract MessageDao messageModel();
    public abstract ConversationDao conversationModel();

    public static Context context;

    public static AppDatabase getInstance() {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context, AppDatabase.class, "spexmesaj").allowMainThreadQueries().build();
        }
        return INSTANCE;
    }

    public void insertMessage(Message message) {

        if (message.isGroup) {
            UserInfo.database.groupModel().setDeleted(false, message.To);
        } else {
            UserInfo.database.conversationModel().setDeleted(false, message.ConversationID);
        }

        messageModel().insertMessage(message);
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}