package com.spexco.msgframework;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import com.google.gson.GsonBuilder;
import com.spexco.msgframework.db.AppDatabase;
import com.spexco.msgframework.ui.splash.SplashActivity;
import com.spexco.msgframework.utils.GsonDateFormatAdapter;
import com.spexco.msgframework.utils.UserInfo;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

import static com.spexco.msgframework.utils.UserInfo.context;
import static com.spexco.msgframework.utils.UserInfo.database;
import static com.spexco.msgframework.utils.UserInfo.getUserID;
import static com.spexco.msgframework.utils.Utility.isValid;

public class MessageApplication extends Application {

    public static Handler mainHandler;
    public static boolean waitingIsGroup = false;
    public static String waitingForUniqueID = "";

    @Override
    public void onCreate() {
        super.onCreate();

        mainHandler = new Handler(Looper.getMainLooper());

        AppDatabase.context = getApplicationContext();

        UserInfo.context = getApplicationContext();
        UserInfo.gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapter(Date.class, new GsonDateFormatAdapter())
                .create();


        database = AppDatabase.getInstance();

        String userID = getUserID();
        if ( isValid(userID) ) {
            UserInfo.myContact = database.contactModel().getContactById(userID);
            database.conversationModel().initConversationWriting();
            database.conversationModel().initGroupConversationWriting();
        }
    }

    public static String getVersionName() {
        try {
            return UserInfo.context.getPackageManager().getPackageInfo(UserInfo.context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void generateNotification(String title, String message, String uniqueID, String msg) {

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(context, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("message", msg);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, getID(), intent, 0);

        String CHANNEL_ID = "my_channel_01";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_ID, importance);

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mChannel);
            }
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.notification)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true);

        Notification notification = builder.build();
        notificationManager.notify(uniqueID, getID(), notification);
    }

    private final static AtomicInteger c = new AtomicInteger(0);
    public static int getID() {
        return c.incrementAndGet();
    }

}