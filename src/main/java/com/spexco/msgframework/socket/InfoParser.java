package com.spexco.msgframework.socket;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.spexco.msgframework.db.ContactWriting;
import com.spexco.msgframework.db.Group;
import com.spexco.msgframework.db.GroupContactWriting;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.db.Participant;
import com.spexco.msgframework.utils.Consts;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.utils.Utility;
import com.spexco.msgframework.db.MessageStatus;

import java.util.List;

/**
 * Created by fatihinan on 4.12.2018.
 */
public class InfoParser extends BaseParser  {

    @Override
    public void parse(JsonObject json) {

        String task = json.get(Consts.KEY_TASK).getAsString();

        if (task.contentEquals(Consts.VALUE_TASK_UPDATE_PROFILE)) {

            ResultParser.ContactResult result = UserInfo.gson.fromJson(json, ResultParser.ContactResult.class);
            UserInfo.database.contactModel().insertContact(result.Data);

        } else if (task.contentEquals(Consts.VALUE_TASK_UPDATE_GROUP)) {

            Group group = UserInfo.gson.fromJson(json.getAsJsonObject("Data"), Group.class);
            UserInfo.database.groupModel().insertGroup(group);

        } else if (task.contentEquals(Consts.VALUE_TASK_START_TYPING) || task.contentEquals(Consts.VALUE_TASK_STOP_TYPING)) {

            StartStopWritingInfo result = UserInfo.gson.fromJson(json, StartStopWritingInfo.class);

            if (result.isGroup != null && result.isGroup) {
                UserInfo.database.conversationModel().setGroupWriting(new GroupContactWriting(result.To, result.From, result.Task.contentEquals(Consts.VALUE_TASK_START_TYPING)));
            } else {
                UserInfo.database.conversationModel().setConversationWriting(new ContactWriting(result.From, result.Task.contentEquals(Consts.VALUE_TASK_START_TYPING)));
            }

        } else if (task.contentEquals(Consts.VALUE_TASK_UPDATE_MESSAGE)) {

            Message message = UserInfo.gson.fromJson(json.getAsJsonObject(Consts.VALUE_TYPE_MESSAGE), Message.class);
            UserInfo.database.messageModel().insertMessage(message);
            if (message.isGroup) {
                notifyListener(UpdateType.MESSAGE_STATUS, message.MessageID);
            } else {
                notifyListener(UpdateType.MESSAGE, message.ConversationID);
            }

        } else if (task.contentEquals(Consts.VALUE_TASK_UPDATE_MESSAGE_STATUS)) {

            MessageStatus messageStatus = UserInfo.gson.fromJson(json, MessageStatus.class);

            if (messageStatus.Status == Consts.MESSAGE_STATUS_SEEN && messageStatus.UserID.contentEquals(UserInfo.myUserID)) {
                UserInfo.database.messageModel().setMessageSeen(messageStatus.MessageID);
            } else {
                messageStatus.initPK();
                UserInfo.database.messageStatusModel().insertMessageStatus(messageStatus);
                notifyListener(UpdateType.MESSAGE_STATUS, messageStatus.MessageID);
            }

        } else if (task.contentEquals(Consts.VALUE_TASK_CREATE_GROUP)) {

            CreateGroupInfo groupInfo = UserInfo.gson.fromJson(json.getAsJsonObject(Consts.KEY_GROUP), CreateGroupInfo.class);
            groupInfo.insertToDB();
            notifyListener(UpdateType.ROLE, groupInfo.GroupID);

        } else if (task.contentEquals(Consts.VALUE_TASK_GROUP_ADD_PARTICIPANT)) {

            CreateGroupInfo groupInfo = UserInfo.gson.fromJson(json, CreateGroupInfo.class);
            groupInfo.insertParticipants();
            notifyListener(UpdateType.ROLE, groupInfo.GroupID);

        } else if (task.contentEquals(Consts.VALUE_TASK_GROUP_REMOVE_PARTICIPANT)) {

            CreateGroupInfo groupInfo = UserInfo.gson.fromJson(json, CreateGroupInfo.class);
            groupInfo.removeParticipants();
            notifyListener(UpdateType.ROLE, groupInfo.GroupID);

        } else if (task.contentEquals(Consts.VALUE_TASK_MAKE_ADMIN) || task.contentEquals(Consts.VALUE_TASK_REMOVE_ADMIN) || task.contentEquals(Consts.VALUE_TASK_MAKE_OWNER)) {

            MakeRemoveAdmin makeRemoveAdmin = UserInfo.gson.fromJson(json, MakeRemoveAdmin.class);
            makeRemoveAdmin.apply();
            notifyListener(UpdateType.ROLE, makeRemoveAdmin.GroupID);

        } else if (task.contentEquals(Consts.VALUE_TASK_UPDATE_CONVERSATION)) {

            List<String> messageIDs;
            ResultParser.MessageStatusInfo msi = UserInfo.gson.fromJson(json, ResultParser.MessageStatusInfo.class);
            if (msi.isGroup) {
                messageIDs = UserInfo.database.messageStatusModel().userGetMessageStatus(msi.UserID, msi.UniqueID, msi.Status);
                if (msi.Status == Consts.MESSAGE_STATUS_DELIVERED) {
                    UserInfo.database.messageStatusModel().userSetMessageStatusDelivered(msi.UserID, msi.UniqueID, msi.Status, msi.UpdateDate);
                } else if (msi.Status == Consts.MESSAGE_STATUS_SEEN) {
                    UserInfo.database.messageStatusModel().userSetMessageStatusSeen(msi.UserID, msi.UniqueID, msi.Status, msi.UpdateDate);
                }

            } else {
                messageIDs = UserInfo.database.messageModel().userGetMessageStatus(msi.UserID, msi.UniqueID, msi.Status);
                if (msi.Status == Consts.MESSAGE_STATUS_DELIVERED) {
                    UserInfo.database.messageModel().userSetMessageStatusDelivered(msi.UserID, msi.UniqueID, msi.Status, msi.UpdateDate);
                } else if (msi.Status == Consts.MESSAGE_STATUS_SEEN) {
                    UserInfo.database.messageModel().userSetMessageStatusSeen(msi.UserID, msi.UniqueID, msi.Status, msi.UpdateDate);
                }
            }

            if (messageIDs != null && messageIDs.size() > 0) {
                for (String messageID : messageIDs) {
                    notifyListener(UpdateType.MESSAGE_STATUS, messageID);
                }
            }

        }
    }

    public class CreateGroupInfo extends Group {

        @Expose
        List<String> Participants;
        @Expose
        List<String> Admins;
        @Expose
        List<String> Deleteds;

        public void insertToDB() {

            isDeleted = false;
            UserInfo.database.groupModel().insertGroup(this);

            insertParticipants();
            insertAdmins();
            removeDeleteds();

            if (Utility.isValid(Owner)) {
                Participant owner = new Participant(GroupID, Owner, Consts.PARTICIPANT_ROLE_OWNER);
                UserInfo.database.participantModel().insertParticipant(owner);
            }
        }

        public void insertParticipants() {
            if (Participants != null) {
                for (String p : Participants) {
                    Participant participant = new Participant(GroupID, p, Consts.PARTICIPANT_ROLE_DEFAULT);
                    UserInfo.database.participantModel().insertParticipant(participant);
                }
            }
        }

        public void removeParticipants() {
            if (Participants != null) {
                for (String p : Participants) {
                    Participant participant = new Participant(GroupID, p, Consts.PARTICIPANT_ROLE_DELETED);
                    UserInfo.database.participantModel().insertParticipant(participant);
                }
            }
        }

        public void removeDeleteds() {
            if (Deleteds != null) {
                for (String p : Deleteds) {
                    Participant participant = new Participant(GroupID, p, Consts.PARTICIPANT_ROLE_DELETED);
                    UserInfo.database.participantModel().insertParticipant(participant);
                }
            }
        }

        public void insertAdmins() {
            if (Admins != null) {
                for (String p : Admins) {
                    Participant participant = new Participant(GroupID, p, Consts.PARTICIPANT_ROLE_ADMIN);
                    UserInfo.database.participantModel().insertParticipant(participant);
                }
            }
        }
    }

    public class StartStopWritingInfo {
        @Expose
        String Task;
        @Expose
        String From;
        @Expose
        String To;
        @Expose
        Boolean isGroup;
    }

    public class MakeRemoveAdmin {
        @Expose
        String GroupID;
        @Expose
        String UserID;
        @Expose
        String Task;

        public void apply() {
            if (Task.contentEquals(Consts.VALUE_TASK_MAKE_ADMIN)) {
                Participant participant = new Participant(GroupID, UserID, Consts.PARTICIPANT_ROLE_ADMIN);
                UserInfo.database.participantModel().insertParticipant(participant);
            } else if (Task.contentEquals(Consts.VALUE_TASK_REMOVE_ADMIN)) {
                Participant participant = new Participant(GroupID, UserID, Consts.PARTICIPANT_ROLE_DEFAULT);
                UserInfo.database.participantModel().insertParticipant(participant);
            } else if (Task.contentEquals(Consts.VALUE_TASK_MAKE_OWNER)) {
                Participant participant = new Participant(GroupID, UserID, Consts.PARTICIPANT_ROLE_OWNER);
                UserInfo.database.participantModel().insertParticipant(participant);
            }
        }
    }
}
