package com.spexco.msgframework.socket;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.spexco.baselibrary.utils.SharedPrefUtil;
import com.spexco.msgframework.utils.Consts;
import com.spexco.msgframework.utils.GsonDateFormatAdapter;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.utils.Utility;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Calendar;

/**
 * Created by fatihinan on 4.12.2018.
 */
public class SpexMesajSocketClient {
    private static SpexMesajSocketClient ourInstance = null;
    private static JsonParser jsonParser;
    private static MessageParser messageParser;
    private static ResultParser resultParser;
    private static InfoParser infoParser;

    public static SpexMesajSocketClient getInstance() {
        if (ourInstance == null) {
            ourInstance = new SpexMesajSocketClient();
            jsonParser = new JsonParser();
            messageParser = new MessageParser();
            resultParser = new ResultParser();
            infoParser = new InfoParser();
        }
        return ourInstance;
    }

    private WebSocketClient webSocketClient;
    public SpexMesajSocketConnectionListener socketConnectionListener;

    private SpexMesajSocketClient() {
    }

    public boolean isConnected() {
        return webSocketClient != null && webSocketClient.isOpen();
    }

    public boolean send(String message) {
        try {
            webSocketClient.send(message);
            Utility.saveString(Consts.KEY_UPDATE_DATE, GsonDateFormatAdapter.sdf.format(Calendar.getInstance().getTime()));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean connect(final String userID) {
        try {
            String loginToken = SharedPrefUtil.getInstance(UserInfo.context).getString(Consts.KEY_LOGIN_TOKEN);
            if (!Utility.isValid(loginToken)) {
                if (socketConnectionListener != null) {
                    socketConnectionListener.onSocketConnectionClose();
                }
                return false;
            }

            String key = "spexmesaj" + userID + UserInfo.source;
            String sec = Utility.hashKeyForDisk(key).toLowerCase();

            String uri = Consts.WS_ADDRESS + "?userid=" + userID + "&sec=" + sec + "&source=" + UserInfo.source + "&token=" + loginToken;

            webSocketClient = new WebSocketClient( new URI(uri), new Draft_6455() ) {

                @Override
                public void onMessage(final String message ) {
                    Log.e("testtest receive -> ", message);

                    JsonObject json = (JsonObject) jsonParser.parse(message);
                    String type = json.get(Consts.KEY_TYPE).getAsString();

                    if (type.contentEquals(Consts.VALUE_TYPE_MESSAGE)) {
                        messageParser.parse(json);
                    } else if (type.contentEquals(Consts.VALUE_TYPE_RESULT)) {
                        resultParser.parse(json);
                    } else if (type.contentEquals(Consts.VALUE_TYPE_INFO) || type.contentEquals(Consts.VALUE_TYPE_SET)) {
                        infoParser.parse(json);
                    }

                    Utility.saveString(Consts.KEY_UPDATE_DATE, GsonDateFormatAdapter.sdf.format(Calendar.getInstance().getTime()));
                }

                @Override
                public void onMessage(ByteBuffer message) {
                    Log.e("testtest", "onMessage ByteBuffer ->" + message);
                }

                @Override
                public void onOpen(ServerHandshake handshake ) {
                    Log.e("testtest", "You are connected To ChatServer: " + getURI());
                }

                @Override
                public void onClose(int code, String reason, boolean remote ) {
                    Log.e("testtest", "You have been disconnected From: " + getURI() + "; Code: " + code + " " + reason);
                    if (socketConnectionListener != null) {
                        socketConnectionListener.onSocketConnectionClose();
                    }

                    if (Utility.isValid(UserInfo.myUserID)) {
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Log.e("testtest", "reconnect");
                                SpexMesajSocketClient.getInstance().connect(UserInfo.myUserID);
                            }
                        }, 5000);
                    }
                }

                @Override
                public void onError( Exception ex ) {
                    Log.e("testtest", "Exception occured ...\n" + ex);
                }
            };

            webSocketClient.connect();

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public interface SpexMesajSocketConnectionListener {
        void onSocketConnectionOpen();
        void onSocketConnectionClose();
    }

}
