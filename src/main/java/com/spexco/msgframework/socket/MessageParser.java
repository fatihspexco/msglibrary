package com.spexco.msgframework.socket;

import com.google.gson.JsonObject;
import com.spexco.msgframework.MessageApplication;
import com.spexco.msgframework.db.Conversation;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.db.MessageStatus;
import com.spexco.msgframework.db.Participant;
import com.spexco.msgframework.ui.chat.ChatActivity;
import com.spexco.msgframework.ui.main.ConversationFragment;
import com.spexco.msgframework.utils.Consts;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.utils.Utility;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by fatihinan on 4.12.2018.
 */
public class MessageParser extends BaseParser  {

    @Override
    public void parse(JsonObject json) {

        Message message = UserInfo.gson.fromJson(json, Message.class);

        if (!message.From.contentEquals(UserInfo.myUserID) && !(message.isGroup != null && message.isGroup)) {
            Conversation conversation = UserInfo.database.conversationModel().getConversation(message.From);
            if (conversation == null) {
                conversation = new Conversation(message.From, message.ConversationID, message.CreateDate);
                UserInfo.database.conversationModel().insertConversation(conversation);
            }
        }

        if (!message.From.contentEquals(UserInfo.myUserID) && message.Status < Consts.MESSAGE_STATUS_DELIVERED) {
            message.Status = Consts.MESSAGE_STATUS_DELIVERED;
            message.DeliverDate = new Date();
            Utility.updateMessage(message);
        }

        if (message.isGroup && message.From.contentEquals(UserInfo.myUserID)) {
            message.Status = Consts.MESSAGE_STATUS_SEND;
            List<Participant> participants = UserInfo.database.participantModel().getParticipants(message.To);
            ArrayList<MessageStatus> messageStatusList = new ArrayList<>();
            for (Participant p : participants) {
                if (!p.UserID.contentEquals(UserInfo.myUserID)) {
                    MessageStatus m = new MessageStatus(message.MessageID, p.UserID, Consts.MESSAGE_STATUS_SEND, null, null, message.To);
                    m.initPK();
                    messageStatusList.add(m);
                }
            }
            UserInfo.database.messageStatusModel().insertMessageStatus(messageStatusList);
        }

        UserInfo.database.insertMessage(message);

        notifyListener(UpdateType.MESSAGE, message.isGroup ? message.To : message.ConversationID);

        //mesaj benden değilse ve (chat açık değilse veya grup chat açık ama mesaj başka gruptansa veya birebir chat açık ama mesaj başka bir kişidense)
        if (!message.From.contentEquals(UserInfo.myUserID) && (!ChatActivity.isOpen || (message.isGroup && !message.To.contentEquals(ChatActivity.uniqueID)) || (!message.isGroup && !message.ConversationID.contentEquals(ChatActivity.uniqueID)))) {

            String visibleName = UserInfo.database.contactModel().getContactVisibleName(message.From);
            if (message.isGroup) {
                visibleName = UserInfo.database.groupModel().getGroupVisibleName(message.To) + ": " + visibleName;
            }
            if (message.MessageType == Consts.MESSAGE_TYPE_TEXT) {
                MessageApplication.generateNotification(visibleName, message.Body, message.isGroup ? message.To : message.ConversationID, json.toString());
            } else if (message.MessageType == Consts.MESSAGE_TYPE_PICTURE) {
                MessageApplication.generateNotification(visibleName, message.Body.length() > 0 ? message.Body : "Resim", message.isGroup ? message.To : message.ConversationID, json.toString());
            } else if (message.MessageType == Consts.MESSAGE_TYPE_VIDEO) {
                MessageApplication.generateNotification(visibleName, message.Body.length() > 0 ? message.Body : "Video", message.isGroup ? message.To : message.ConversationID, json.toString());
            } else if (message.MessageType == Consts.MESSAGE_TYPE_LOCATION) {
                MessageApplication.generateNotification(visibleName, "Konum", message.isGroup ? message.To : message.ConversationID, json.toString());
            } else if (message.MessageType == Consts.MESSAGE_TYPE_CONTACT) {
                MessageApplication.generateNotification(visibleName, "Kişi", message.isGroup ? message.To : message.ConversationID, json.toString());
            }
        }

        if (ConversationFragment.isOpen) {
            ConversationFragment.instance.updateConversations();
        }
    }

}
