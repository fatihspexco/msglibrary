package com.spexco.msgframework.socket;

import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.spexco.msgframework.MessageApplication;
import com.spexco.msgframework.db.Contact;
import com.spexco.msgframework.db.Conversation;
import com.spexco.msgframework.db.Message;
import com.spexco.msgframework.ui.main.MainActivity;
import com.spexco.msgframework.utils.Consts;
import com.spexco.msgframework.utils.UserInfo;
import com.spexco.msgframework.utils.Utility;
import com.spexco.msgframework.db.MessageStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by fatihinan on 4.12.2018.
 */
public class ResultParser extends BaseParser  {

    @Override
    public void parse(JsonObject json) {

        boolean success = json.get(Consts.KEY_SUCCESS).getAsBoolean();
        if (!success) {
            return;
        }

        String task = json.get(Consts.KEY_TASK).getAsString();

        if (task.contentEquals(Consts.VALUE_TASK_UPDATED_DATA)) {

            UpdateDataResult result = UserInfo.gson.fromJson(json, UpdateDataResult.class);
            UserInfo.database.contactModel().insertContact(result.Contacts);
            UserInfo.database.conversationModel().insertConversation(result.Conversations);

            if (result.Groups != null) {
                for (InfoParser.CreateGroupInfo group : result.Groups) {
                    group.insertToDB();
                }
            }

            result.insertMessageStatuses();

            result.insertMessages();

            Utility.saveString(Consts.KEY_UPDATE_DATE, result.UpdateDate);

            if (MainActivity.isOpen) {
                MainActivity.instance.versionControl(result.Version);
            } else {
                MainActivity.version = result.Version;
            }

        } else if (task.contentEquals(Consts.VALUE_TASK_CONVERSATION_HISTORY)) {

            UpdateDataResult result = UserInfo.gson.fromJson(json, UpdateDataResult.class);

            result.insertMessageStatuses();

            result.insertMessages();

            if (result.Messages != null && result.Messages.size() > 0) {
                Message message = result.Messages.get(0);
                notifyListener(UpdateType.MESSAGE, message.isGroup ? message.To : message.ConversationID);
            }

        } else if (task.contentEquals(Consts.VALUE_TASK_LOGIN)) {

            ContactResult result = UserInfo.gson.fromJson(json, ContactResult.class);
            UserInfo.setUserID(result.Data.UserID);
            UserInfo.myContact = result.Data;

            UserInfo.sendToSocket(UserInfo.getUpdatedDataJsonString());

            UserInfo.database.contactModel().insertContact(result.Data);
            if (SpexMesajSocketClient.getInstance().socketConnectionListener != null) {
                SpexMesajSocketClient.getInstance().socketConnectionListener.onSocketConnectionOpen();
            }

            for (Message message : UserInfo.database.messageModel().getUnsyncMessages()) {
                if (UserInfo.sendToSocket(UserInfo.gson.toJson(message, Message.class))) {
                    if (message.Status == Consts.MESSAGE_STATUS_WAITING) {
                        message.Status = Consts.MESSAGE_STATUS_SEND;
                    }
                    message.isSync = true;
                }
                UserInfo.database.messageModel().insertMessage(message);
            }
        } else if (task.contentEquals(Consts.VALUE_TASK_UPDATE_PROFILE)) {
            MessageApplication.mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(UserInfo.context, "Profil başarıyla güncellendi.", Toast.LENGTH_SHORT).show();
                }
            });
        } else if (task.contentEquals(Consts.VALUE_TASK_UPDATE_GROUP)) {
            MessageApplication.mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(UserInfo.context, "Grup bilgisi başarıyla güncellendi.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public class UpdateDataResult {
        @Expose
        public String UpdateDate;
        @Expose
        public ArrayList<Contact> Contacts;
        @Expose
        public ArrayList<Conversation> Conversations;
        @Expose
        public ArrayList<Message> Messages;
        @Expose
        public ArrayList<InfoParser.CreateGroupInfo> Groups;
        @Expose
        public ArrayList<MessageStatus> MessageStatuses;
        @Expose
        public Version Version;

        public void insertMessages() {

            boolean seenInit = Utility.getString(Consts.KEY_INIT_DATA,"-1").contentEquals("-1");
            Utility.saveString(Consts.KEY_INIT_DATA, "1");

            List<String> uniqueIDs = new ArrayList<>();
            List<MessageStatusInfo> info = new ArrayList<>();

            for (Message message : Messages) {

                if (seenInit && message.isGroup) {
                    message.Status = Consts.MESSAGE_STATUS_SEEN;
                }

                if (!message.From.contentEquals(UserInfo.myUserID) && message.Status < Consts.MESSAGE_STATUS_DELIVERED) {

                    message.Status = Consts.MESSAGE_STATUS_DELIVERED;
                    message.DeliverDate = new Date();

                    String uniqueID = message.isGroup ? message.To : message.ConversationID;
                    String contactID = message.isGroup ? "" : message.From;
                    if (!uniqueIDs.contains(uniqueID) && !UserInfo.myUserID.contentEquals(contactID)) {
                        uniqueIDs.add(uniqueID);
                        info.add(new MessageStatusInfo(uniqueID, message.isGroup, Consts.MESSAGE_STATUS_DELIVERED, contactID));
                    }
                }
            }

            if (info.size() > 0) {
                Utility.updateConversation(info);

                for (MessageStatusInfo msi : info) {
                    if (msi.isGroup) {
                        UserInfo.database.groupModel().setDeleted(false, msi.UniqueID);
                    } else {
                        UserInfo.database.conversationModel().setDeleted(false, msi.UniqueID);
                    }
                }
            }

            UserInfo.database.messageModel().insertMessage(Messages);
        }

        public void insertMessageStatuses() {
            if (MessageStatuses != null) {
                for (MessageStatus m : MessageStatuses) {
                    m.initPK();
                }
                UserInfo.database.messageStatusModel().insertMessageStatus(MessageStatuses);
            }
        }
    }

    public static class MessageStatusInfo {
        @Expose
        public String UserID;
        @Expose
        public String UniqueID;
        @Expose
        public boolean isGroup;
        @Expose
        public int Status;
        @Expose
        public String ContactID;
        @Expose
        public Date UpdateDate;

        public MessageStatusInfo(String UniqueID, boolean isGroup, int Status, String ContactID) {
            this.UniqueID = UniqueID;
            this.isGroup = isGroup;
            this.Status = Status;
            this.ContactID = ContactID;
            this.UpdateDate = new Date();
        }
    }

    public class Version {

        @Expose
        public Double Version;

        @Expose
        public String Link;

        @Expose
        public String Description;

        @Expose
        public Boolean isForced;
    }

    public class ContactResult {
        @Expose
        public Contact Data;
    }


}
