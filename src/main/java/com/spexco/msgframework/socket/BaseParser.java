package com.spexco.msgframework.socket;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by fatihinan on 29.01.2019.
 */
public abstract class BaseParser {

    abstract void parse(JsonObject json);

    private static List<UpdateListener> listeners = new ArrayList<>();

    public static void addListener(UpdateListener listener) {
        listeners.add(listener);
    }

    public static void removeListener(UpdateListener listener) {
        listeners.remove(listener);
    }

    void notifyListener(UpdateType type, String uniqueID) {
        for (UpdateListener listener : listeners) {
            if (listener != null && listener.getUniqueID().contentEquals(uniqueID)) {
                listener.onUpdate(type);
            }
        }
    }

    public interface UpdateListener {
        void onUpdate(UpdateType type);
        String getUniqueID();
    }

    public enum UpdateType {
        MESSAGE,
        MESSAGE_STATUS,
        ROLE
    }
}
